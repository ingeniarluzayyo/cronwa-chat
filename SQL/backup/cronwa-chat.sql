-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-09-2015 a las 02:03:51
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cronwa-chat`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answer`
--

CREATE TABLE IF NOT EXISTS `answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` text NOT NULL,
  `message` text NOT NULL,
  `type` text NOT NULL,
  `user` int(11) NOT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date` datetime NOT NULL,
  `type` text NOT NULL,
  `sender_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `campaign`
--

INSERT INTO `campaign` (`id`, `content`, `date`, `type`, `sender_id`) VALUES
(2, 'aaaaa', '2015-09-09 20:45:19', 'text', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `message` text CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `sender_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `type` varchar(20) NOT NULL,
  `sent` varchar(1) NOT NULL,
  `mode` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `chat`
--

INSERT INTO `chat` (`id`, `campaign_id`, `message`, `sender_id`, `contact_id`, `datetime`, `type`, `sent`, `mode`) VALUES
(2, 2, 'aaaaa', 3, 1, '2015-09-09 20:45:19', 'text', '0', 'sent');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `number` bigint(20) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `number`, `sender_id`, `date_create`, `status`) VALUES
(1, 'luciano colucci', 5491121692730, 3, '2015-09-08 00:00:00', 'Suscribe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hash_media`
--

CREATE TABLE IF NOT EXISTS `hash_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(65) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `hash_media`
--

INSERT INTO `hash_media` (`id`, `hash`, `fecha`) VALUES
(1, 'Psa06QmTvmTPE8yiIVghgvLGlhip2AIb76JzgUA1NWo=', '2015-08-13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credits` int(11) NOT NULL,
  `amount` text NOT NULL,
  `bg_color` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `packages`
--

INSERT INTO `packages` (`id`, `credits`, `amount`, `bg_color`) VALUES
(1, 10000, '1000', 'bg-red');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages_x_user`
--

CREATE TABLE IF NOT EXISTS `packages_x_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL,
  `package` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `senders`
--

CREATE TABLE IF NOT EXISTS `senders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` text NOT NULL,
  `password` text NOT NULL,
  `user` int(11) NOT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  `status` text NOT NULL,
  `profile_change` varchar(4) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `senders`
--

INSERT INTO `senders` (`id`, `numero`, `password`, `user`, `habilitado`, `status`, `profile_change`) VALUES
(3, '5491153439942', 'qR+VJtFx500HmSRp8RmyEG7OYzo=', 2, '1', 'pause', 'no'),
(4, '573118081044', 'xLMVriSmBHaiqsRF7/3395EWKFY=', 0, '1', 'pause', 'no'),
(5, '573166884781', 'Qs3En59OIBFQcOPDuV/tje80LWw=', 0, '1', 'pause', 'no'),
(6, '573195244393', 'AoWIgiDB3fy+/LveikG5rFo/PKE=', 0, '1', 'pause', 'no'),
(7, '5491169293093', 'nbIaqNbNjSbEXc2nYnXFHCMgZpc=', 0, '1', 'pause', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` text NOT NULL,
  `currency` text NOT NULL,
  `timezone` text NOT NULL,
  `whatsapp_version` text NOT NULL,
  `hora_inicio` text NOT NULL,
  `hora_fin` text NOT NULL,
  `sleep_time_between_msg` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `currency`, `timezone`, `whatsapp_version`, `hora_inicio`, `hora_fin`, `sleep_time_between_msg`) VALUES
(1, 'CronWhatsApp', '$', 'America/Argentina/Buenos_Aires', '2.11.16', '8:00', '21:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(512) NOT NULL,
  `email` text NOT NULL,
  `telefono` text NOT NULL,
  `user_type` varchar(300) NOT NULL,
  `credits` int(11) NOT NULL DEFAULT '0',
  `alta_word` text NOT NULL,
  `baja_word` text NOT NULL,
  `default_answer` text NOT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `password`, `email`, `telefono`, `user_type`, `credits`, `alta_word`, `baja_word`, `default_answer`, `habilitado`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', 'admin', 0, '', '', '', '1'),
(2, 'test', '098f6bcd4621d373cade4e832627b4f6', '', '', '', 0, 'Suscribe', 'Unsuscribe', 'Invalid command', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
