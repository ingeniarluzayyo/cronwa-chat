-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-10-2015 a las 00:20:18
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cronwa-chat`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `senders`
--

CREATE TABLE IF NOT EXISTS `senders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` text NOT NULL,
  `password` text NOT NULL,
  `user` int(11) NOT NULL,
  `habilitado` varchar(1) NOT NULL DEFAULT '1',
  `status` text NOT NULL,
  `profile_change` varchar(4) NOT NULL DEFAULT 'no',
  `encuesta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `senders`
--

INSERT INTO `senders` (`id`, `numero`, `password`, `user`, `habilitado`, `status`, `profile_change`, `encuesta_id`) VALUES
(3, '5491153439942', 'qR+VJtFx500HmSRp8RmyEG7OYzo=', 2, '1', 'waiting', 'no', 16);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
