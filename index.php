<?php
include_once("lib/variables.php");
require_once("lib/clases.php");
include_once("lib/textos_estaticos.php");
session_start();

LoginIngeniar::VerificarLogin();

if(isset($_GET['logout']) && $_GET['logout']==1)
{				
	LoginIngeniar::Logout();
}

//Tomar apartado a seleccionar por POST o GET
if(isset($_GET["apartado"]))
{
	$apartado = $_GET["apartado"];
}

if(isset($_POST["apartado"]))
{
	$apartado = $_POST["apartado"];
}

//Control de Pie y Menu
require_once("controls/body/body_control.php");

if(isset($apartado))
{	
	$seccion_seleccionada = $apartado;
	
	switch($apartado)
	{
		case "home":
		{
			require_once("controls/home/home_control.php");
			break;
		}
		
		case "settings":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/settings/settings_control.php");
			}else
			{
				require_once("controls/settings/settings_user_control.php");
			}
			break;
		}
		
		case "registration":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/registration/registration_control.php");
			}
			break;
		}
		case "registration_sender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/registration/registration_sender_control.php");
			}
			break;
		}
		
		case "bulkmessage":
		{
			require_once("controls/bulkmessage/bulkmessage_control.php");
			break;
		}
		
		//Answer
		case "newanswer":
		{
			if($user->modulo_answer || $user->IsAdmin($_SESSION['id_user'])){
				require_once("controls/answer/newAnswer_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}	
			break;
			
		}
		case "answer":
		{
			if($user->modulo_answer || $user->IsAdmin($_SESSION['id_user'])){
				require_once("controls/answer/modAnswer_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		//Messages
		case "campaigns":
		{
			if($user->modulo_campaign || $user->IsAdmin($_SESSION['id_user'])){
				require_once("controls/messages/lstMessagesSended_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		//Contacts
		case "contacts":
		{
			require_once("controls/contacts/contacts_control.php");
			break;
		}
		
		//Chat
		case "chat":
		{
			if($user->modulo_chat || $user->IsAdmin($_SESSION['id_user'])){
				require_once("controls/contacts/chat_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		//Encuesta
		case "newsurvey":
		{
			if($user->modulo_survey || $user->IsAdmin($_SESSION['id_user'])){
				require_once("controls/encuesta/newEncuesta_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "survey":
		{
			if($user->modulo_survey || $user->IsAdmin($_SESSION['id_user'])){
				require_once("controls/encuesta/encuesta_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
			
		//Usuarios
		case "newuser":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/usuarios/newUser_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "moduser":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/usuarios/modUser_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		//Usuarios
		case "usercredits":
		{
			require_once("controls/usuarios/userCredits_control.php");
			break;
		}
		
		//Senders
		case "newsender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/senders/newSender_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "modsender":
		{
			
			require_once("controls/senders/modSender_control.php");
			break;
		}
		
		case "importsender":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/senders/importSender_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		//Package
		case "packages":
		{
			require_once("controls/packages/Package_control.php");

			break;
		}
		
		case "pendingcredits":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/packages/PendingCredits_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		
		case "newpackage":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/packages/newPackage_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
		case "modpackage":
		{
			if($user->IsAdmin($_SESSION['id_user']))
			{
				require_once("controls/packages/modPackage_control.php");
			}else
			{
				$seccion_seleccionada = 'home';
				require_once("controls/home/home_control.php");
			}
			break;
		}
			
		case "404":
		{
			require_once("controls/404/404_control.php");
			break;
		}

		default:
		{
			$seccion_seleccionada = 'home';
			require_once("controls/home/home_control.php");
			break;
		}
	}
}else{
	$seccion_seleccionada = 'home';
	require_once("controls/home/home_control.php");	
}





?>
