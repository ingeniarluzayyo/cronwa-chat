<?php
	/************************
	
	Nombre: Clases
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	DESCRIPCION:
	Contiene la inclucion de todas las librerias de clases para utilizar.
		
	*************************/
	
//Clases generles
require_once("class/Database.class.php");
require_once("class/ManejoArchivos.class.php");
require_once("class/ManejoTextos.class.php");
require_once("class/Formularios.class.php");
require_once("class/ChatFactory.class.php");
require_once("class/Tablas.class.php");
require_once("class/LoginIngeniar.class.php");
require_once("config.php");

?>