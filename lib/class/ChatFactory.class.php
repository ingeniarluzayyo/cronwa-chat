<?php

class ChatFactory
{
	/************************
	
	Nombre: Chat
	Version: 1.0	
	Fecha de creacion: 02/09/2015
	Autor: Martin
	Fecha de ultima modificacion: 02/09/2015
	Autor de ultima modificacion: Martin


	METODOS:

	
	*************************/
	
	private $chat;

	public function __construct() {
			$this->chat 			= '';
		}
		
		
	public function cabecera($contact)
	{
		$this->chat .= '<div class="row">
							<div id="cabecera" class="col-md-4">
							<img id="adjunto_msg" src="img/clip.png" onclick="modalShow();">
							<p>'.$contact.'</p>
							
							</div>
						</div>';
	}
	

	public function InicioChat()
	{
		$this->chat .= '<div class="row">
							<div id="container" class="col-md-4">	
								<div id="subcontainer" class="col-md-12">';
	}
	
	public function Mensaje($chatObject) 
	{
		if($chatObject->mode == "sent"){
			$this->chat .= '<div class="row">
							<div class="col-md-12">
								<div class="container_msj_sender">
									<div class="msj_sender">';
		if($chatObject->type == "text"){
						$this->chat .=	'<p>'.nl2br($chatObject->message).'</p>';	
		}								
		if($chatObject->type == "image"){
			
						$this->chat .=	'<img class="multimedia" src="message_record'.$this->GetUrl($chatObject->message).'" />';
						$this->chat .=	'<p>'.nl2br($this->GetCaption($chatObject->message)).'</p>';	
		}
		if($chatObject->type == "video"){
			
						$this->chat .=	'<video class="multimedia" src="message_record'.$this->GetUrl($chatObject->message).'" />';
						$this->chat .=	'<p>'.nl2br($this->GetCaption($chatObject->message)).'</p>';	
		}
		if($chatObject->type == "audio"){
			
						$this->chat .=	'<audio class="multimedia" src="message_record'.$this->GetUrl($chatObject->message).'" />';	
		}								
		
		if($chatObject->sent == 1){
						$this->chat .=			'<img src="img/tilde.png" class="tilde">';
		}else{
						$this->chat .=			'<img src="img/timer.png" class="timer">';
		}							
				$this->chat .=		    '<p class="pull-right hora">'.$chatObject->datetime.'</p>
										<div class="clear"></div>
									</div>
									<div class="flecha_sender"></div>
								</div>
							</div>
						</div>';
		}else{
			$this->chat .= '<div class="row">
							<div class="col-md-12">
								<div class="container_msj_contacto">
									<div class="msj_contacto">';
			if($chatObject->type == "text"){
							$this->chat .=	'<p>'.$chatObject->message.'</p>';	
			}								
			if($chatObject->type == "image"){
				
							$this->chat .=	'<img src="message_record'.$this->GetUrl($chatObject->message).'" />';
							$this->chat .=	'<p>'.$this->GetCaption($chatObject->message).'</p>';	
			}
			if($chatObject->type == "video"){
				
							$this->chat .=	'<video src="message_record'.$this->GetUrl($chatObject->message).'" />';
							$this->chat .=	'<p>'.$this->GetCaption($chatObject->message).'</p>';	
			}
			if($chatObject->type == "audio"){
				
							$this->chat .=	'<audio src="message_record'.$this->GetUrl($chatObject->message).'" />';	
			}
									
							$this->chat .=	'<p class="pull-right hora">'.$chatObject->datetime.'</p>
									<div class="clear"></div>
									</div>
									<div class="flecha_contacto"></div>
								</div>
							</div>
						</div>';
		}
		
	}

	public function FinChat()
	{
		$this->chat .= '		</div>
								<div class="clear"></div>
								<textarea id="input_wa"></textarea>
								<img id="send_button" onclick="Mensaje();" src="img/send.png"/>
							</div>
						</div>';
	}
	
	public function GenerarChat(){
		echo $this->chat;
	}
	
	public function GetChat(){
		return $this->chat;
	}
	
	private function GetUrl($content){
		$array_contenido = explode('message_record',$content);
		$array_contenido = explode(';',$array_contenido[1]);
		$contenido = $array_contenido[0];
      	
  		return $contenido;
	}
	
	private function GetCaption($content){
		$array_contenido = explode('message_record',$content);
		$array_contenido = explode(';',$array_contenido[1]);
      	
      	//por si tiene acentos y eso con ;
		$caption = $array_contenido[1]; 
		if(count($array_contenido)>2){
			for($i=2;$i<count($array_contenido);$i++){
				$caption .= ';'.$array_contenido[$i];
			}
		}
		
      	return $caption;
	}
	
}

?>