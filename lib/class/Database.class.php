<?php
class Database {
	/************************
	
	Nombre: Database
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	METODOS:

	Database::getInstance() {Devuelve la instancia de la base de datos.}
	Database::getInstance()->GetUnicoQuery($query);
	Database::getInstance()->executeQuery($query);
	Database::getInstance()->insert($arr, $tabla);
	Database::getInstance()->update($Arr, $Tabla, $Where);
	Database::getInstance()->delete($Tabla, $Where);
	Database::getInstance()->Cadena ($parametro); {Pone entre ' el parametro pasado}
	Database::getInstance()->SQLiProtect ($parametro); {Proteje de inyecciones SQL}
	
	*************************/

    static private $instance = null;

   	private $DataBase_host 		= "localhost";
	private $DataBase_Usuario 	= "root";
	private $DataBase_Pass 		= "root";
	private $DataBase_Name		= "cronwa-chat";
	private $DataBase_Port		= "";
	private $conn;
	
    
	private function __contruct() {
	}
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }
    /********* CONEXION Y DESCONEXION BASE DE DATOS *********/
	public function connect() {
		$this->conn = new mysqli($this->DataBase_host, $this->DataBase_Usuario, $this->DataBase_Pass, $this->DataBase_Name);
		if($this->conn) {
			return true;
		}
		return false;
    }
	
	public function disconnect() {
		$this->conn->close();
    }
    
	
		/*** AGREGAR MODIFICAR O ELIMINAR ****/
	
	public function insert($arr, $tabla) {
		$find = array('•','°','º','á', 'é', 'í', 'ó', 'ú', 'ñ','Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
		$replace = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
		
		$campo = "";
		$valor = "";
		foreach ($arr as $Indice=>$Info) {
			$valor .= $Info . ", ";
			$campo .= $Indice . ", ";
		}		
		$campo = substr($campo, 0, -2);
		$valor = substr($valor, 0, -2);
		
		$query 	= "INSERT INTO ";
		$query .= $tabla;
		$query .=  " (" . $campo . ") ";
		$query .=  "VALUES ";
		$query .=  " (" . Database::SQLiProtect(str_ireplace($find,$replace,$valor)) . "); ";

		if(!$this->executeQuery($query)) {
			return false;
		}
		
		return true;
	}
	
	public function insert_bulk($arr, $tabla) {
		$find = array('•','°','º','á', 'é', 'í', 'ó', 'ú', 'ñ','Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
		$replace = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
		
		$campo = "";
		$valor = "";
		
		foreach($arr as $a){
			foreach ($a as $Indice=>$Info) {
					$campo .= $Indice . ", ";
			}
			break;
		}
				
		$campo = substr($campo, 0, -2);
		$valor = substr($valor, 0, -2);
		
		$query 	= "INSERT INTO ";
		$query .= $tabla;
		$query .=  " (" . $campo . ") ";
		$query .=  "VALUES ";
		foreach($arr as $a){
			$i=1;
			foreach ($a as $Indice=>$Info) {
					if(count($a)!=$i)
					{
						$valor .= $Info . ", ";	
					}else{
						$valor .= $Info;
					}
					$i++;		
			}
			$query .=  " (" . Database::SQLiProtect(str_ireplace($find,$replace,$valor)) . "),";
			$valor = "";
		}
		$query = substr ($query, 0, -1);
		$query .=  "; ";
		
		if(!$this->executeQuery($query)) {
			return false;
		}
		return true;
	}

	
	public function update($Arr, $Tabla, $Where) {
		$find = array('•','°','º','á', 'é', 'í', 'ó', 'ú', 'ñ','Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
		$replace = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
		
		$CampoUpdate = "";
		foreach ($Arr as $Indice=>$Info) {
			$CampoUpdate.= $Indice . " = " . Database::SQLiProtect(str_ireplace($find,$replace,$Info)) . ", ";
		}		
		
		$CampoUpdate = substr($CampoUpdate, 0, -2);
		
		$Where =  str_ireplace($find,$replace,$Where); 
		
		$query 	= "UPDATE ";
		$query .= $Tabla . " ";
		$query .= "SET ";
		$query .= $CampoUpdate . " ";
		$query .= $Where;
		
		//echo $query;
		if(!$this->executeQuery($query)) {
			return false;
		}
		return true;
	}

	public function delete($Tabla, $Where) {
		$find = array('•','°','º','á', 'é', 'í', 'ó', 'ú', 'ñ','Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
		$replace = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
				
		$Where =  str_ireplace($find,$replace,$Where);
		
		$query 	= "DELETE FROM ";
		$query .= $Tabla . " ";
		$query .= $Where;
		
		if(!$this->executeQuery($query)) {
			return false;
		}
		return true;
	}
	
	/***** EJECUCION DE QUERYS ************/
	
	public function executeQuery($query) {
		if (!$this->connect()) {
			echo "No se pudo establecer conexión";
		} else {
			$Resultado = $this->conn->query($query);
			$this->disconnect();
			if(!$Resultado) {
				return false;
			}
			return $Resultado;
		}
    }
    
	public function GetUnicoQuery($sql) {		
		$Resultado = $this->executeQuery($sql);
		if (!$Fila = mysqli_fetch_array($Resultado)) {			
			return false;
		}		
		if(($Resultado->num_rows) == 1) {			
			return $Fila; 				
		} else
		return false;
    }
	
	
	
	/*********** MISC FUNCIONES ***************/
	
	public static function Cadena ($Variable) {
		$Variable = "'" . $Variable . "'";
		return $Variable;

	}

	public static function SQLiProtect ($Variable) {
		$Variable = strip_tags($Variable);
		return $Variable;

	}
	
	
}

?>