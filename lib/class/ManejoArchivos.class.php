<?php

class ManejoArchivos
{
	/************************
	
	Nombre: ManejoArchivos
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	METODOS:

	ManejoArchivos::borrar_archivo($nombre,$ruta);
	ManejoArchivos::boton_editar_archivo($nombre, $ruta_relativa, $ancho, $alto , $ruta_disco); -> Crea un boton cuyo action es controls/archivos/archivo.php, donde usualmente se procesa y guarda el archivo.
	ManejoArchivos::boton_editar_foto($nombre, $ruta_relativa, $ancho, $alto , $ruta_disco); -> Crea un boton cuyo action es controls/archivos/imagen.php, donde usualmente se procesa y guarda la foto.
	ManejoArchivos::control_documento($tipo_archivo); -> controla que el tipo de archivo del documento sea valido ($_FILE['archivo']['type']), retorna la extension sin "." ej: pdf
	ManejoArchivos::control_documento($tipo_archivo); -> controla que el tipo de archivo de la foto sea valida ($_FILE['archivo']['type']), retorna la extension sin "." ej: jpg
	ManejoArchivos::existe_documento($nombre , $ruta); -> Comprueba si existe algun documento con las extensiones validas ($extenciones_documentos) en esa ruta, y retorna $nombre.extencion ej: archivo.pdf
	ManejoArchivos::existe_foto($nombre , $ruta); -> Comprueba si existe alguna foto con las extensiones validas ($extenciones_fotos) en esa ruta, y retorna $nombre.extencion ej: archivo.jpg
	ManejoArchivos::guardar_archivo($archivo,$ruta,$nombre,$tipo_archivo); -> Guarda el archivo $archivo en la $ruta con el nombre $nombre y la extension del $tipo_archivo. **IMPORTANTE- solo si el archivo viene de un upload
	ManejoArchivos::guardar_archivo($archivo,$ruta,$nombre,$tipo_archivo); -> Guarda la FOTO $archivo en la $ruta con el nombre $nombre y la extension del $tipo_archivo. **IMPORTANTE- solo si el archivo viene de un upload
	ManejoArchivos::guardar_documento($archivo,$ruta,$nombre,$tipo_archivo); -> Guarda el DOCUMENTO $archivo en la $ruta con el nombre $nombre y la extension del $tipo_archivo. **IMPORTANTE- solo si el archivo viene de un upload
	ManejoArchivos::move_foto($archivo,$ruta,$nombre,$tipo_archivo); -> Mueve la FOTO $archivo a la $ruta con el $nombre y la extension del $tipo_archivo.
	ManejoArchivos::move_documento($archivo,$ruta,$nombre,$tipo_archivo); -> Mueve el DOCUMENTO $archivo a la $ruta con el $nombre y la extension del $tipo_archivo. 
	
	$extenciones_fotos: Extenciones validas de fotos
	$extenciones_documentos: Extenciones validas de documentos
	
	*************************/
	
	static $extenciones_fotos = array("jpg","png","gif","jpeg");
	static $extenciones_documentos = array("pdf","doc","docx");

	public static function existe_foto( $nombre , $ruta )
	{	   
	   foreach(ManejoArchivos::$extenciones_fotos as $extencion)
	   {	   		   		
	   		if(file_exists($ruta.$nombre.".".$extencion))
	   		{	
	   			return $nombre.".".$extencion;
	   		}	   		
	   }
		return "sin_foto.jpg";		
	}

	public static function existe_documento( $nombre , $ruta )
	{	   
	   foreach(ManejoArchivos::$extenciones_documentos as $extencion)
	   {
	   		if(file_exists($ruta.$nombre.".".$extencion))
	   		{	
	   			return $nombre.".".$extencion;
	   		}	   		
	   }
		return "";		
	}

	public static function control_foto($tipo_archivo)
	{		
		//Control de extencion
		if ($tipo_archivo == "image/jpg" ){
	        $tipo_archivo = "jpg";
		}else if( $tipo_archivo == "image/png"){
		    $tipo_archivo = "png";
		}else if( $tipo_archivo == "image/jpeg"){
		    $tipo_archivo = "jpg";
		}else if ($tipo_archivo == "image/gif" ){
		    $tipo_archivo = "gif";
		}else{
			$tipo_archivo ="";
			echo "<script language='JavaScript'>alert('La extencion del archivo no es valida.');</script>";
		}

		return $tipo_archivo;

	}
	
	public static function control_video($tipo_archivo)
	{		
		//Control de extencion
		if ($tipo_archivo == "video/mp4" ){
	        $tipo_archivo = "mp4";
		}else{
			$tipo_archivo ="";
			echo "<script language='JavaScript'>alert('La extencion del archivo no es valida.');</script>";
		}

		return $tipo_archivo;

	}
	
	public static function control_audio($tipo_archivo)
	{		
		//Control de extencion
		if ($tipo_archivo == "audio/mp3" || $tipo_archivo == "audio/mpeg" ){
	        $tipo_archivo = "mp3";
		}else{
			$tipo_archivo ="";
			echo "<script language='JavaScript'>alert('La extencion del archivo no es valida.');</script>";
		}

		return $tipo_archivo;

	}
	
	public static function control_documento($tipo_archivo)
	{		
		//Control de extencion
		if ($tipo_archivo == "application/pdf" ){
	        $tipo_archivo = "pdf";
		}else if( $tipo_archivo == "application/msword"){
		    $tipo_archivo = "doc";
		}else if( $tipo_archivo == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"){
		    $tipo_archivo = "docx";
		}else if ($tipo_archivo == "text/plain" ){
		    $tipo_archivo = "txt";
		}else{
			
			echo "<script language='JavaScript'>alert('La extencion del archivo no es valida".$tipo_archivo."');</script>";
			$tipo_archivo ="";
			exit();
		}

		return $tipo_archivo;

	}

	public static function guardar_archivo($archivo,$ruta,$nombre,$tipo_archivo,$tipo)
	{
		if($tipo == "imagen")
		{
			$nombre .= ".".ManejoArchivos::control_foto($tipo_archivo);	
		}
		
		if($tipo == "video")
		{
			$nombre .= ".".ManejoArchivos::control_video($tipo_archivo);	
		}
		
		if($tipo == "audio")
		{
			$nombre .= ".".ManejoArchivos::control_audio($tipo_archivo);	
		}
			

		if(move_uploaded_file($archivo, $ruta.$nombre))
		{
			//echo "<script language='JavaScript'>alert('El archivo se guardo correctamente.');</script>";
			return true;
		}else{
			//echo "<script language='JavaScript'>alert('Ocurrio un problema al guardar el archivo.');</script>";
			return false;
		}
	}
	
	public static function move_foto($archivo,$ruta,$nombre,$tipo_archivo)
	{
		$nombre .= ".".ManejoArchivos::control_foto($tipo_archivo);		

		if(rename($archivo, $ruta.$nombre))
		{			
			return true;
		}else{			
			return false;
		}
	}
	
	public static function guardar_documento($archivo,$ruta,$nombre,$tipo_archivo)
	{
		$nombre .= ".".ManejoArchivos::control_documento($tipo_archivo);		

		if(move_uploaded_file($archivo, $ruta.$nombre))
		{			
			return true;
		}else{			
			return false;
		}
	}
	
	public static function move_documento($archivo,$ruta,$nombre,$tipo_archivo)
	{
		$nombre .= ".".ManejoArchivos::control_documento($tipo_archivo);		

		if(rename($archivo, $ruta.$nombre))
		{			
			return true;
		}else{			
			return false;
		}
	}

	public static function borrar_archivo($nombre,$ruta)
	{
		$archivo_a_borrar ="";

   			
		$archivo_a_borrar = ManejoArchivos::existe_foto($nombre,$ruta);



	  /* if($archivo_a_borrar == "")
	   {
	   		foreach(ManejoArchivos::$extenciones_documentos as $extencion)
		   {
		   		if(file_exists($ruta.$nombre.".".$extencion))
		   		{	
		   			$archivo_a_borrar = $nombre.".".$extencion;
		   		}	   			   		
		   }
	   }*/

	   if($archivo_a_borrar != "")
	   {
		   	if (unlink($ruta.$archivo_a_borrar))
	  		{	
	  			echo "<script language='JavaScript'>alert('El archivo se borro correctamente.');</script>";
				return true;
	  		}else{
	  			echo "<script language='JavaScript'>alert('Ocurrio un problema al borrar el archivo.');</script>";
				return false;
	  		}	  		
	  	}else{
	  		echo "<script language='JavaScript'>alert('No se encontro el archivo a borrar el archivo.');</script>";
			return false;
	  	}
	}
	public static function boton_editar_foto($nombre, $ruta_relativa, $ancho, $alto , $ruta_disco)
	{

		if(ManejoArchivos::existe_foto($nombre,$ruta_disco) != "")
		{
			$boton_editar_foto = '<input class="btn btn-default" name="txt_foto_editar" value="Editar foto"  type="button" class="boton" '; 
			$boton_editar_foto .= 'onClick="ventanaXY(200, 200, '.$ancho.', '.$alto.',';
			$boton_editar_foto .= "'controls/imagenes/imagen.php?nombre=". $nombre . "&ruta=" . $ruta_relativa."&ruta_disco=".$ruta_disco."&txt_titulo=Edición de fotografia' ,'')";
			$boton_editar_foto .= '">';
		}else{
			$boton_editar_foto = '<input class="btn btn-default" name="txt_foto_editar" value="Crear foto"  type="button" class="boton" '; 
			$boton_editar_foto .= 'onClick="ventanaXY(200, 200,'.$ancho.','.$alto.',';
			$boton_editar_foto .= "'controls/imagenes/imagen.php?nombre=". $nombre . "&ruta=" . $ruta_relativa ."&ruta_disco=".$ruta_disco."&txt_titulo=Creación de fotografia' ,'')";
			$boton_editar_foto .= '">';
		}

		return $boton_editar_foto;
	}
	
	public static function boton_editar_archivo($nombre, $ruta_relativa, $ancho, $alto , $ruta_disco)
	{

		if(ManejoArchivos::existe_documento($nombre,$ruta_disco) != "")
		{
			$boton_editar_foto = '<input class="btn btn-default" name="txt_foto_editar" value="Editar archivo"  type="button" class="boton" '; 
			$boton_editar_foto .= 'onClick="ventanaXY(200, 200, '.$ancho.', '.$alto.',';
			$boton_editar_foto .= "'controls/archivos/archivo.php?nombre=". $nombre . "&ruta=" . $ruta_relativa."&ruta_disco=".$ruta_disco."&txt_titulo=Edición de archivo' ,'')";
			$boton_editar_foto .= '">';
		}else{
			$boton_editar_foto = '<input class="btn btn-default" name="txt_foto_editar" value="Crear archivo"  type="button" class="boton" '; 
			$boton_editar_foto .= 'onClick="ventanaXY(200, 200,'.$ancho.','.$alto.',';
			$boton_editar_foto .= "'controls/archivos/archivo.php?nombre=". $nombre . "&ruta=" . $ruta_relativa ."&ruta_disco=".$ruta_disco."&txt_titulo=Creación de archivo' ,'')";
			$boton_editar_foto .= '">';
		}

		return $boton_editar_foto;
	}
	
	public static function contenido_archivo($nombre_fichero)
	{	   
		$fichero_texto = fopen ($nombre_fichero, "r");
		   
		//OJO! Debido a filesize(), sólo funcionará con archivos de texto
		$contenido_fichero = fread($fichero_texto, filesize($nombre_fichero));
		return $contenido_fichero;
	}
}

?>