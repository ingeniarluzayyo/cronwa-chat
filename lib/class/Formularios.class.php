<?php

class Formularios
{
	/************************
	
	Nombre: Formularios
	Version: 1.0	
	Fecha de creacion: 11/10/2014
	Autor: Martin
	Fecha de ultima modificacion: 11/10/2014
	Autor de ultima modificacion: Martin


	METODOS:

	Formularios::inicioForm();
	Formularios::inicioFormFile();
	Formularios::addCheckBox();
	Formularios::addClean();
	Formularios::addComboBox();
	Formularios::addHiddenInput();
	Formularios::addInput();
	Formularios::addInputFecha();
	Formularios::addInputPassword();
	Formularios::addLinkButton();
	Formularios::addSubmit();
	Formularios::addTextArea();
	
	*************************/
	
	public $form;
	public $m;

	public function __construct() {
			$this->form 			= '';
			$this->m 			= '';
		}
		
	public function alert($msg,$class)
	{
		if(!empty($msg))
		{
			if($class === false){
				$class='alert-danger';
			}
			if($class === true){
				$class='alert-success';
			}
			$this->form .= '
	                <div class="alert '.$class.' alert-dismissable">
	                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                  '.$msg.'
	                </div>   
	        ';
        }
	}
		
	public function cabecera($titulo,$msg)
	{
		if(!empty($msg)){
			$this->form .= '

		<div class="row">
          <div class="col-lg-12">
            <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              '.$msg.'
            </div>
          </div>
        </div><!-- /.row -->         
        ';
		}
		
	}
	

	public function inicioForm($metodo,$action)
	{
		$this->form .= '
					<div class="row">
			          <div class="col-lg-12">
			              <div class="col-lg-8">
			                <form role="form" method="'.$metodo.'" action="'.$action.'">
						';
	}
	
	public function inicioFormFile($metodo,$action)
	{
		$this->form .= '
					<div class="row">
			          <div class="col-lg-12">
			              <div class="col-lg-6">
			                <form role="form" method="'.$metodo.'" action="'.$action.'" enctype="multipart/form-data">
						';
	}

	public function finForm()
	{
		$this->form .= '
								</form>
							</div>
          				</div><!-- /.row -->

						';
	}

	public function generarForm()
	{			   
	    echo $this->form;
	}

	public function addSubmit($label,$name,$value)
	{
		$this->form .=  '
			<button type="submit" class="btn btn-default" name="'.$name.'" value="'.$value.'">'.$label.'</button>
        ';
	}
	
	public function addLinkButton($label,$link)
	{
		$this->form .=  '
		<div class="form-group">
			<div class="botones_columna"><a class="btn btn-default" target="_blank" href="'.$link.'">'.$label.'</a></div>
		</div>
        ';
	}

	public function addClean($label)
	{
		$this->form .=  '
			<button type="reset" class="btn btn-default">'.$label.'</button>
        ';
	}

	public function addTextArea($label,$name,$value)
	{
		$this->form .=  '
			<div class="form-group">
                <label>'.$label.'</label>
                <textarea class="form-control" rows="3" name="'.$name.'">'.$value.'</textarea>
              </div>
        ';
	}

	public function addInput($label,$name,$value,$type)
	{
		$this->form .=  '
			<div class="form-group">
                <label>'.$label.'</label>
                <input type="'.$type.'" class="form-control" placeholder="Enter data" name="'.$name.'" id="'.$name.'" value="'.$value.'">
              </div>
        ';
	}
	
	public function addLabel($label,$id)
	{
		$this->form .=  '
			<div class="form-group" id="'.$id.'">
                <label>'.$label.'</label>                
              </div>
        ';
	}

	public function addInputFecha($label,$name,$value)
	{
		$this->form .=  '
			<div class="form-group">
                <label>'.$label.'</label>
                <input type="date" class="form-control" placeholder="Ingrese fecha dd/mm/aa" name="'.$name.'" value="'.$value.'">
              </div>
        ';
	}
	
	public function addInputDatetime($label,$name,$value)
	{
		$this->form .=  '
			<div class="form-group">
                <label>'.$label.'</label>
                <input type="datetime" class="form-control" placeholder="Ingrese fecha dd/mm/aa" name="'.$name.'" value="'.$value.'">
              </div>
        ';
	}

	public function addCheckBox($label,$name,$value,$checked)
	{
		$this->form .=  '
			<div class="checkbox">
                <label>
                  <input type="checkbox" value="'.$value.'" name="'.$name.'" ';
                  if($checked){$this->form .= 'checked';}
        			
        			$this->form .=  ' >                      
                  '.$label.'
                </label>
              </div>
        ';
	}

	public function addHiddenInput($name,$value)
	{
		$this->form .= '
			<input type="hidden" name="'.$name.'" value="'.$value.'"/>
        ';
	}
	
	public function addComboBox($label,$name,$arr,$seleccionada,$script)//$arr["value"=>"valor_a_mostrar"]
	{
		$selected = "";
		//ej script: onchange="javascript:id_superior_onChange(this.value);"
		$this->form .=  '
			<div class="form-group">
                <label>'.$label.'</label>
	              <select class="form-control" name="'.$name.'" onchange="javascript:'.$script.'">
	              	<option value="0"></option>
				  	';
				  	if(!empty($arr)){
					  	foreach($arr as $key=>$value)
					  	{
					  		if($seleccionada == $key){$selected = "selected";};										
							$this->form .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
							$selected = "";
						}								
					}		  	
				  	$this->form .='
				  </select>
			</div>
        ';
	}
	
	public function inicioModal($id)
	{
		$this->m = '
		<div class="modal fade" id="'.$id.'" tabindex="-1" role="dialog" aria-labelledby="contact-form" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-right: 10px;margin-top: 5px;">
                    <span aria-hidden="true">x</span>
                </button>
                <div class="modal-body">';
	}
	
	public function contenidoFormulario($contenido)
	{
		$this->form .= $contenido;
	}
	
	public function finModal()
	{
		$this->m .= '
			</div> <!-- *** end modal-body *** -->
	            </div> <!-- *** end modal-content *** -->
	        </div> <!-- *** end modal-dialog *** -->
	    </div> <!-- *** end Contact Form modal *** -->
		';
	}
	
	public function contenidoModal($contenido)
	{
		$this->m .= $contenido;
	}
	
		public function generarModal()
	{			   
	    echo $this->m;
	}
	
	public function generarBotonModal($id,$clase,$boton)
	{
		echo '<a class="btn btn-primary '.$clase.'" data-toggle="modal" data-target="#'.$id.'">'.$boton.'</a>';
	}
}

?>