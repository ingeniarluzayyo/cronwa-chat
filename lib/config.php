<?php
set_time_limit (0);
mb_internal_encoding("UTF-8");

require_once(__DIR__."/../models/Settings.class.php");
$settings = new Settings();
$settings->GetById(1);
date_default_timezone_set($settings->timezone);

define('CANTIDAD_ITEMS_PAGINA',100);
define('SUSCRIBE_STATUS','Suscribe');
define('UNSUSCRIBE_STATUS','Unsuscribe');


define('STATUS_PAUSE','pause');
define('STATUS_WAITING','waiting');
define('STATUS_RUNNING','running');
define('STATUS_LOGIN_FAIL','login fail');
?>