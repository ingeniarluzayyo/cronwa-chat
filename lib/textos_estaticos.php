<?php
/************************
	
Nombre: Textos Estaticos
Version: 1.0	
Fecha de creacion: 11/10/2014
Autor: Martin
Fecha de ultima modificacion: 11/10/2014
Autor de ultima modificacion: Martin


DESCRIPCION:
Contiene la definicion de las variables estaticas segun idiomas de la seccion de la web.
	
*************************/
$seccion_seleccionada = '';

if(!isset($idioma) || $idioma == "es")
{
	//MENU CABECERA		
	$nav_home = 'Home';
	$nav_nosotros = 'Nosotros';
	$nav_servicios = 'Servicios';
	$nav_portfolio = 'Portfolio';
	$nav_promociones = 'Promociones';
	$nav_faq = 'FAQ';
	$nav_contacto = 'Contacto';
	$foot_newsletter = 'Inserte su email';
	$foot_suscribirse = "Suscribase&ensp;!";
	
	//TEXTOS GENERALES
	$mas_info = "M&aacute;s info";
	$titulo_404 = "Error 404, p&aacute;gina no encontrada";
	$descripcion_404 = "La p&aacute;gina que esta buscando no existe.";
	
	//FOOTER
	$foot_nosotros = "Nosotros";
	$foot_sitemap = "Sitemap";
	$foot_redes_sociales = "Redes sociales";
	$foot_contacto = "Contacto";
	
	$foot_nav_home = 'Home';
	$foot_nav_nosotros = 'Nosotros';
	$foot_nav_servicios = 'Servicios';
	$foot_nav_portfolio = 'Portfolio';
	$foot_nav_promociones = 'Promociones';
	$foot_nav_contacto = 'Contacto';
	$foot_nav_faq = 'FAQ';
	$foot_nav_privacidad = 'Pol&iacute;ticas de privacidad';

	//SERVICIOS
	$servicios_titulo = "Que ofrecemos a nuestros clientes:";
	
	//PORTFOLIO
	$portfolio_todos = "Todos";
	
}else if($idioma == "en"){
	
	$nav_inicio = 'HOME';
	$nav_producto = 'PRODUCT';
	$nav_tecnologia = 'TECHNOLOGY';
	$nav_ferias = 'EVENTS';
	$nav_empresa = 'COMPANY';
	$nav_noticias = 'NEWS';
	$nav_contacto = 'CONTACT';
	$todas = "ALL";
	$no_hay_productos = "No products loaded";
}else{		
	$nav_inicio = 'INICIO';
	$nav_producto = 'PRODUCTO';
	$nav_tecnologia = 'TECNOLOGÍA';
	$nav_ferias = 'FERIAS';
	$nav_empresa = 'EMPRESA';
	$nav_noticias = 'NOTICIAS';
	$nav_contacto = 'CONTACTO';
	$todas = "TODAS";
	$no_hay_productos = "No hay productos cargados";
}

?>