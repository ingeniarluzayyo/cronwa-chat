function Chat()
{
	//Variables estaticas utilizadas por AJAX asincronicos
	Chat.mensajes_nuevos_globales = [];
	
	//Variables
	this.id					= '';
	this.campaign_id		= '';
	this.message 			= '';
	this.sender_id			= '';
	this.contact_id			= '';
	this.datetime 			= '';
	this.type				= '';
	this.sent				= '';
	this.mode				= '';
	
	//Metodos
	
	/**Inicializar*/
	this.inicializar = function inicializar(id,campaign_id,message,sender_id,contact_id,datetime,type,sent,mode)
	{
		this.id = id;
		this.campaign_id = campaign_id;
		this.message = message;
		this.sender_id = sender_id;
		this.contact_id = contact_id;
		this.datetime = datetime;
		this.type = type;
		this.sent = sent;
		this.mode = mode;
	}
	
	/**ParseoDeArray*/
	this.parseoDeArray = function parseoDeArray(mensaje_en_formato_array)
	{	
		this.inicializar(mensaje_en_formato_array.id,mensaje_en_formato_array.campaign_id,mensaje_en_formato_array.message,mensaje_en_formato_array.sender_id,mensaje_en_formato_array.contact_id,mensaje_en_formato_array.datetime,mensaje_en_formato_array.type,mensaje_en_formato_array.sent,mensaje_en_formato_array.mode); 		
	}
	
	/**Enviar*/
	this.enviar = function enviar()
	{				
		$.ajax({
            url: url_root + "index.php",
            type: 'post',            
            async:true,                        
            data: "apartado=chat&message="+ this.message + "&sender_id=" + this.sender_id + "&contact_id=" + this.contact_id +"&type=" + this.type,
        	success: function(data){
				if(data != 'error'){
					document.getElementById("input_wa").value = "";
					$('#subcontainer').html(data);
					var objDiv = document.getElementById("subcontainer");
					objDiv.scrollTop = objDiv.scrollHeight;
				};        
			}
        });  
	         
	}
	
	/**Enviar Multimedia*/
	this.enviarMultimedia = function enviarMultimedia()
	{	
		this.type = $('input:radio[name=tipo]:checked').val();
		
		var adjunto =  $("#" + this.type)[0].files[0];
		/*var adjunto = $('#imagen').get(0).files[0]*/
		
		var data = new FormData();
	
		data.append('apartado','chat');
		data.append('sender_id',this.sender_id);
		data.append('contact_id',this.contact_id);
		data.append('type',this.type);
		
		if(this.type == 'imagen'){
			data.append('imagen',adjunto);
			data.append('caption_imagen',document.getElementById("caption_imagen").value);
		}
		if(this.type == 'video'){
			data.append('video',adjunto);
			data.append('caption_video',document.getElementById("caption_video").value);
		}
		if(this.type == 'audio'){
			data.append('audio',adjunto);
		}

		
		$.ajax({
				type: "POST",
				data: data,
        		url:  url_root + "index.php",
				cache: false,
				enctype: 'multipart/form-data',
		        processData: false, // Don't process the files
		        contentType: false,
		        dataType: "json",
				success: function(datos){

					   if(datos.success == true){
							$('#subcontainer').html(datos.message);
							var objDiv = document.getElementById("subcontainer");
							objDiv.scrollTop = objDiv.scrollHeight;
							$('#myModal').modal('hide');
					   }else{
					   		$('#error_msg').show();
					   		$('#error').text(datos.message);
					   }
				}					
					
       	 });  
	         
	}
	
	/**Actualizar*/
	this.actualizar = function actualizar(contact)
	{				
		$.ajax({
            url: url_root + "index.php",
            type: 'post',            
            async:true,                        
            data: "apartado=chat&actualizar=actualizar&contact_id=" + contact,
        	success: function(data){
        			if(data == 'error survey'){
						survey_active();
						return;
					}else{
						survey_desactive();
					}
        	
					var objDiv = document.getElementById("subcontainer");
					
					if(objDiv.scrollTop == objDiv.scrollHeight){
						$('#subcontainer').html(data);
						var objDiv = document.getElementById("subcontainer");
						objDiv.scrollTop = objDiv.scrollHeight;
					}else{
						$('#subcontainer').html(data);
					}	
			}
        });  
 	         
	}
}