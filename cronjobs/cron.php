<?php
set_time_limit(0);
require_once(__DIR__.'/../lib/variables.php');
require_once(__DIR__.'/../lib/clases.php');

require_once(__DIR__.'/../lib/api/whatsapp/whatsprot.class.php');
require_once(__DIR__.'/../models/Encuesta.class.php');
require_once(__DIR__.'/../models/Opcion.class.php');
require_once(__DIR__.'/../models/ContactoXEncuesta.class.php');
require_once(__DIR__.'/../models/Contact.class.php');
require_once(__DIR__.'/../models/Sender.class.php');
require_once(__DIR__.'/../models/Usuario.class.php');
require_once(__DIR__.'/../models/Answer.class.php');
require_once(__DIR__.'/../models/Message.class.php');
require_once(__DIR__.'/../models/HashMedia.class.php');
require_once(__DIR__.'/../models/Chat.class.php');

$contact = new Contact();
$sender = new Sender();
$chat = new Chat();
$message = new Messages();
$encuesta = new Encuesta();

$sender->GetFirstWaiting();

if($sender->id == 0){exit();}

var_dump($sender->numero);

try{
	$w = new WhatsProt($sender->numero, "WhatsApp", false);
	$w->eventManager()->bind("onGetMessage", "GetMessage");			

	$w->Connect();			
	$w->LoginWithPassword($sender->password);
	
	$sender->Update_status(STATUS_RUNNING);
}catch(LoginFailureException $e_l){
	$sender->Update_status(STATUS_LOGIN_FAIL);
	exit();
}

try{	
	$contador = 0;
	while(true){
		try{
			$w->PollMessage();
		}catch(ConnectionException $e_c){	
			sleep(10);

			$w->Connect();			
			$w->LoginWithPassword($sender->password);
			
			echo "Error ConnectionException\n";
		}
		
		sleep(1);
		
		//FIJARSE SI TIENE Q ENVIAR ALGUN MENSAJE
		if($chat->GetEnvio($sender->id) != false){
			
			$contact->GetById($chat->contact_id,$sender->user);
			
			if($message->Send($sender->user,$contact,$chat->type,$chat->message,$sender,$w)){
				//update enviado
				$chat->Update_sent();
			}
		}
		

		
		if($contador > 30){$contador = 0;}
		
		if($contador == 30)
		{
			//CHECK FINALIZACION ENCUESTA
			if($sender->CheckEncuesta() != 0){
				$encuesta->GetById($sender->encuesta_id);
				
				if($encuesta->finish_date < $encuesta->creation_date){
					$sender->Update_encuesta(0);
				}
			}
			
			
			// CHECK CAMBIO DE FOTO PERFIL
			if($sender->CheckProfilePicture())
			{
				$path =  __DIR__.PAIT.'..'.PAIT.'img'.PAIT.'profile_picture'.PAIT;
				$foto = ManejoArchivos::existe_foto($sender->id,$path);
				if($foto != 'sin_foto.jpg')
				{	
					$w->sendSetProfilePicture($path.$foto);
				}
				$sender->Update_profile_change('no');
			}
			
			if(!$sender->CheckPause())
			{
				$w->disconnect();
				exit;
			}
		}
				
		$contador++;
	};
}catch(Exception $e){
	$sender->Update_status(STATUS_WAITING);
	echo "Error Exception1".print_r($e,true);
	exit();
}

function GetMessage( $mynumber, $from, $id, $type, $time, $name, $body )
{
	global $w;
	global $contact;
	global $sender;
	
	echo("Message from $name:\n$body\n\n");
	
	
	$numero = explode('@',$from);
	$contact->number = $numero[0];
	$contact->name = $name;
	$contact->sender_id = $sender->id;
	
	$contact->ProcessWord($body,$sender,$w);
}
?>