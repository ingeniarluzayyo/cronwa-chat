<?php
//Llamar al modelo
require_once("models/Usuario.class.php");
require_once("models/Settings.class.php");

$user = new Usuario();
//Manejo de datos
$user->GetById($_SESSION['id_user']);

$settings = new Settings();
$settings->GetById(1);

date_default_timezone_set($settings->timezone);

$titulo_web = $settings->site_name;
?>