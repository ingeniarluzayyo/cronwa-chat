<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/Sender.class.php");
$sender = new Sender();
$value = "";

$arr_user = $user->GetArrayAll(); 

if(isset($_GET['accion']) && $_GET['accion'] == 'start' && $user->IsAdmin($_SESSION['id_user']) && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$sender->GetById($_GET['id']);
	$result = $sender->Update_status(STATUS_WAITING);
}

if(isset($_GET['accion']) && $_GET['accion'] == 'pause' && $user->IsAdmin($_SESSION['id_user']) && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$sender->GetById($_GET['id']);
	$result = $sender->Update_status(STATUS_PAUSE);
}

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && $user->IsAdmin($_SESSION['id_user']) && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$sender->GetById($_GET['id']);
	$result = $sender->Eliminar();
}

if(isset($_POST["profile_picture"]))
{
	$id = $_POST["id"];
	$sender->GetById($id);
	
	require_once("views/senders/profile_picture_view.phtml");
	exit();
}

if(isset($_POST["save_profile_picture"]))
{
	$id = $_POST["id"];
	$sender->GetById($id);
	
	try{
		if($_FILES['profile_img']['type'] != "image/jpeg" && $_FILES['profile_img']['type'] != "image/jpg")
		{
			$result = array();
			$result['state'] = false;
			$result['msg'] = "Error the picture type is not jpg,jpeg.";
		}else
		{
			if(!ManejoArchivos::guardar_archivo($_FILES['profile_img']['tmp_name'],HD_IMAGES.'profile_picture'.PAIT,$id,$_FILES['profile_img']['type'],'imagen'))
			{
				$result = array();
				$result['state'] = false;
				$result['msg'] = "Error while upload de profile picture.";
			}else{
				$sender->Update_profile_change('yes');
				$result = array();
				$result['state'] = true;
				$result['msg'] = "The profile picture will be change soon.";
			}
		}
	}catch(Exception $e)
	{
		$result = array();
		$result['state'] = false;
		$result['msg'] = "Error while upload de profile picture.";
	}
}


if(isset($_POST["editar"]) && $user->IsAdmin($_SESSION['id_user']))
{
	$id = $_POST["id"];

	//Llamar a la vista
	if($sender->GetById($id))
	{
		require_once("views/senders/modSender_view.phtml");
	}
}else if(isset($_POST["aceptar"])){	
	
	$id = $_POST["id"];
	
	$sender->id = $id;
		
	$sender->ParseoDeArray($_POST);

	$result = $sender->Modificar();
	
	$senders = $sender->GetAll();

	require_once("views/senders/lstSenderAdmin_view.phtml");
}else{
	
	if(isset($_POST['search']) && $_POST['number'] != "")
	{
		$value = $_POST['number'];
		$sender->numero = $value;
		if($user->IsAdmin($_SESSION['id_user'])){
			$senders = $sender->GetByNumber();
		}else{
			$senders = $sender->GetByNumber($_SESSION['id_user']);
		}
	}else{
		$value = "";
		
		if($user->IsAdmin($_SESSION['id_user'])){
			$senders = $sender->GetAll();
		}else{
			$senders = $sender->GetAll($_SESSION['id_user']);
		}	
	}
	
		
	//Llamar a la vista
	if($user->IsAdmin($_SESSION['id_user'])){
		require_once("views/senders/lstSenderAdmin_view.phtml");	
	}else{
		require_once("views/senders/lstSender_view.phtml");	
	}
	

}

function onGetProfilePicture($from, $target, $type, $data)
{
    if ($type == "preview") {
        $filename = "preview_" . $target . ".jpg";
    } else {
        $filename = $target . ".jpg";
    }
    //define("PICTURES_FOLDER",'img/profile');
    $filename = 'img/profile'."/" . $filename;
    $fp = @fopen($filename, "w");
    if ($fp) {
        fwrite($fp, $data);
        fclose($fp);
    }
}
?>