<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar a al modelo
require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/Sender.class.php");
require_once("models/Usuario.class.php");

$usuario = new Usuario();
$arr_user = $usuario->GetArrayAll(); 

if(isset($_POST["aceptar"]))
{	
	$sender = new Sender();
	$sender->ParseoDeArray($_POST);
	

	$result = $sender->Crear();
}

//Llamar a la vista
require_once("views/senders/newSender_view.phtml");

?>