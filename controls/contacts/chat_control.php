<?php
require_once("models/Chat.class.php");
require_once("models/Sender.class.php");
require_once("models/Contact.class.php");

$contact = new Contact();
$sender = new Sender();

$chat = new Chat();
$chatFactory = new ChatFactory(); 
$jsondata = array();

if(isset($_POST["message"]) && isset($_POST["type"]) && $_POST["type"] == 'text' && isset($_POST["contact_id"]) && isset($_POST["sender_id"]) && ctype_digit($_POST["contact_id"]) && ctype_digit($_POST["sender_id"]) ){

	$chat->ParseoDeArray($_POST);
	$chat->mode = "sent";
	
	if($chat->message != ''){

		if($chat->Crear()){
		
			if($user->IsAdmin($_SESSION['id_user'])){
				$chats = $chat->GetAll();
			}else{
				$chats = $chat->GetAll($_SESSION['id_user']);
			}
			
			$c = new ChatFactory();
			foreach($chats as $ca){
				$c->Mensaje($ca);
			}
			$c->GenerarChat();
			exit;
		}else{
			echo 'error';
			exit;
		}
	}else{
		echo 'error';
		exit;
	}
}

if(isset($_FILES) && isset($_POST["type"]) && isset($_POST["contact_id"]) && isset($_POST["sender_id"]) && ctype_digit($_POST["contact_id"]) && ctype_digit($_POST["sender_id"]) ){

	//Manejo de archivos y controles de extencion
	$tipo = $_POST['type'];

	switch ($tipo) {
    case 'imagen':
	 	require_once('controls/messages/image.php');	 
        break;
    case 'audio':
        require_once('controls/messages/audio.php');	
        break;
    case 'video':
        require_once('controls/messages/video.php');  
        break;    
	}

	if(!isset($error)){
		$chat->mode = "sent";
		$chat->message = $contenido;
		$chat->sender_id = $_POST["sender_id"];
		$chat->contact_id = $_POST["contact_id"];
		$chat->type = $tipo;
		
		if($chat->message != ''){
			
			if($chat->Crear()){		
				if($user->IsAdmin($_SESSION['id_user'])){
					$chats = $chat->GetAll();
				}else{
					$chats = $chat->GetAll($_SESSION['id_user']);
				}
				
				$c = new ChatFactory();
				foreach($chats as $ca){
					$c->Mensaje($ca);
				}
				
				$jsondata['success'] = true;
	        	$jsondata['message'] = $c->GetChat();
			}else{
				$jsondata['success'] = false;
	        	$jsondata['message'] = 'An error happend while sending the message';
			}
		}else{
				$jsondata['success'] = false;
	        	$jsondata['message'] = 'An error happend while sending the message';
		}
	}else{
		$jsondata['success'] = false;
        $jsondata['message'] = $error;
	}
	

    header('Content-type: application/json; charset=utf-8');
	echo json_encode($jsondata);
	exit();
}

if(isset($_POST["actualizar"]) && isset($_POST["contact_id"]) && ctype_digit($_POST["contact_id"])){
		
		//Control Encuesta Activa
		$contact->GetById($_POST["contact_id"]);
		$sender->GetById($contact->sender_id);
		
		if($sender->encuesta_id != 0){
			echo "error survey";
			exit;
		}
		
		$chat->ParseoDeArray($_POST);
		
		if($user->IsAdmin($_SESSION['id_user'])){
			$chats = $chat->GetAll();
		}else{
			$chats = $chat->GetAll($_SESSION['id_user']);
		}
		
		$c = new ChatFactory();
		foreach($chats as $ca){
			$c->Mensaje($ca);
		}
		$c->GenerarChat();
		
}

?>