<?php
require_once("models/Sender.class.php");
require_once("models/Contact.class.php");
require_once("models/Chat.class.php");

$sender = new Sender();
$contact = new Contact();
$chat = new Chat();
$check = "ordername";


if(isset($_POST["contacts"])){

	$contact->sender_id = $_POST["id"];
	
	$sender->GetById($_POST["id"]);
	
	if(isset($_POST['chat']))
	{		
		$chat->contact_id = $_POST["contact"];

		if($user->IsAdmin($_SESSION['id_user'])){
			$contact->GetById($_POST["contact"]);
			$chats = $chat->GetAll();
		}else{
			$contact->GetById($_POST["contact"],$_SESSION['id_user']);
			$chats = $chat->GetAll($_SESSION['id_user']);
		}
		
		require_once("views/contacts/chat_view.phtml");
		exit;
	}
	
	if(isset($_POST['borrar']))
	{		
		$contact->id = $_POST["contact"];
		
		if($user->IsAdmin($_SESSION['id_user'])){
			$result = $contact->Eliminar();
		}else{
			$result = $contact->Eliminar($_SESSION['id_user']);
		}
	}
	
	if(isset($_POST['search']))
	{
		$contact->name   = $_POST["name"];
		$contact->number = $_POST["number"];
		$contact->status = $_POST["status"];
		$check = $_POST["radio_order"];
	}

	if($user->IsAdmin($_SESSION['id_user'])){
		$contacts = $contact->Search($check);
	}else{
		$contacts = $contact->Search($check,$_SESSION['id_user']);
	}
	
	require_once("views/contacts/contact_list_view.phtml");
	exit;
}


if($user->IsAdmin($_SESSION['id_user'])){
	$senders = $sender->GetAll();
}else{
	$senders = $sender->GetAll($_SESSION['id_user']);
}


require_once("views/contacts/contact_view.phtml");	
?>