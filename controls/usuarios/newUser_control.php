<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Usuario.class.php");

if(isset($_POST["aceptar"]))
{	
	$habilitado = 0;
	if(isset($_POST["habilitado"]) && $_POST["habilitado"] == 1){$habilitado = 1;}

	$usuario = new Usuario();
	$usuario->ParseoDeArray($_POST);
	$usuario->habilitado = $habilitado;

	$result = $usuario->Crear();
}

//Llamar a la vista
require_once("views/usuarios/newUser_view.phtml");

?>