<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar al modelo
require_once("models/Sender.class.php");
require_once("models/Chat.class.php");
require_once("models/Campaign.class.php");
require_once("models/Contact.class.php");
require_once("models/Usuario.class.php");

//Sender
$sender = new Sender();
if($user->IsAdmin($_SESSION['id_user'])){
	$senders = $sender->GetAll();
}else{
	$senders = $sender->GetAll($_SESSION['id_user']);
}

//Manejo de datos
if(isset($_POST['send']) and $_POST['send']=='ok')
{
	$sender = new Sender();
	$user = new Usuario();
	$chat = new Chat();
	$campaing = new Campaign();
	
	if(!is_numeric($_POST['sender']))
	{
		$error = 'Error, must select a sender.';
		require_once("views/bulkmessage/bulkmessage_view.phtml");
		exit;
	}
	
	$sender->GetById($_POST['sender']);
	
	//Validacion Encuesta
	if($sender->encuesta_id != 0){
		$error = "The selected sender has an active survey, you can not send a campaign at this time.";
		require_once("views/bulkmessage/bulkmessage_view.phtml");
		exit;
	}

	$contacto = new Contact();
	$contacto->sender_id = $_POST['sender'];
	$contacto->status = SUSCRIBE_STATUS;
	$contactos = $contacto->Search($_SESSION['id_user']);
	
	
	//Manejo de archivos y controles de extencion
	$tipo = $_POST['tipo'];
	$user->GetByUsername($_SESSION['username']);
	$user_id =  $user->id;
	$cant_total = count($contactos);
	
	switch ($tipo) {
    case 'text':
	    require_once('controls/messages/text.php');
        break;
    case 'imagen':
	 	require_once('controls/messages/image.php');	 
        break;
    case 'audio':
        require_once('controls/messages/audio.php');	
        break;
    case 'video':
        require_once('controls/messages/video.php');  
        break;    
	}
	
	if(!isset($error))
	{
		$campaing->content    = $contenido;
		$campaing->type       = $tipo;
		$campaing->sender_id  = $_POST['sender'];
		$result = $campaing->Crear();

		if($result)
		{
			$chat->campaign_id = $campaing->LastId();
			$chat->message = $contenido;
			$chat->sender_id   = $_POST['sender'];
			$chat->type = $tipo;
			$chat->mode = 'sent';
			
			foreach($contactos as $contacto)
			{	
				$chat->contact_id = $contacto->id;			
				$result &= $chat->CrearBulk();
		 	}
		}
		
		$result &= $chat->Save();	

	}
}
	
	
	
	require_once("views/bulkmessage/bulkmessage_view.phtml");
?>