<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Chat.class.php");
require_once("models/Campaign.class.php");
require_once("models/Usuario.class.php");
require_once("models/Sender.class.php");

$chat = new Chat();
$usuario = new Usuario();
$campaign = new Campaign();
$sender = new Sender();

$from = '';
$to = '';
$users_messages = $usuario->GetAll();
$type = (isset($_GET['type'])? $_GET['type']:'');
$user_messages = ($usuario->IsAdmin($_SESSION['id_user']))?0:$_SESSION['id_user'];

if(isset($_POST["search"]))
{
	$type = (isset($_POST['type'])? $_POST['type']:'');
	$from = (isset($_POST['from'])? $_POST['from']:'');
	$to   = (isset($_POST['to'])? $_POST['to']:'');
	if($usuario->IsAdmin($_SESSION['id_user'])){
		$user_messages = (isset($_POST['user'])? $_POST['user']:'');
	}	
}

$campaigns = $campaign->Search($user_messages,$from,$to);
//Llamar a la vista
require_once("views/messages/lstBulkMessagesSended_view.phtml");	



?>