<?php
	require_once("models/Packages.class.php");
	require_once("models/Packages_x_User.class.php");
	
	$package = new Packages();

	$packages = $package->GetAll();

	if(isset($_POST['id']) && is_numeric($_POST['id']))
	{
		if($package->GetById($_POST['id']))
		{
				$packages_x_user = new Packages_x_User();
				
				$packages_x_user->package = $_POST['id'];
				$packages_x_user->user = $_SESSION['id_user'];
				$packages_x_user->fecha = date("Y-m-d H:i:s");
				
				$result = $packages_x_user->Crear();
				echo $result['msg'];
				exit();
		}	
			
	}


	require_once("views/packages/package_view.phtml");	
?>