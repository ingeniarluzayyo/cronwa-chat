<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar a al modelo
require_once("models/Packages.class.php");

if(isset($_POST["aceptar"]))
{	
	$package = new Packages();
	$package->ParseoDeArray($_POST);
	

	$result = $package->Crear();
}

$arr_bg = array('bg-red'=>'bg-red','bg-yellow'=>'bg-yellow','bg-aqua'=>'bg-aqua','bg-blue'=>'bg-blue','bg-green'=>'bg-green','bg-light-blue'=>'bg-light-blue','bg-navy'=>'bg-navy','bg-teal'=>'bg-teal','bg-olive'=>'bg-olive','bg-lime'=>'bg-lime','bg-orange'=>'bg-orange','bg-fuchsia'=>'bg-fuchsia','bg-purple'=>'bg-purple','bg-maroon'=>'bg-maroon','bg-black'=>'bg-black');

//Llamar a la vista
require_once("views/packages/newPackage_view.phtml");

?>