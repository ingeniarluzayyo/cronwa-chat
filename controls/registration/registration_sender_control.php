<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

require_once("lib/api/whatsapp/whatsprot.class.php");
require_once("models/Sender.class.php");
$sender = new Sender();

if(isset($_POST["aceptar"]) && isset($_POST["numero"]) && isset($_POST["code"]))
{	
	try{			
		$w = new WhatsProt($_POST["numero"], "", false);
		$_POST["code"] = str_replace("-","",$_POST["code"]);
		$response = $w->codeRegister($_POST["code"]);
		
		$sender = new Sender();
		$sender->numero = $response->login;
		$sender->password = $response->pw;
		$sender->status = 'pause';
		$sender->habilitado = '1';
		
		$result = $sender->Crear();
	}catch(Exception $e)
	{
		$result['state'] = false;
		$result['msg'] = "An error happends when registring a sender.<br />".$e->getMessage();	
	}
	
	
}

require_once("views/registration/registration_sender_view.phtml");

?>
