<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar al modelo
require_once("models/Answer.class.php");

$answer = new Answer();

if(isset($_GET['accion']) && $_GET['accion'] == 'borrar' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$answer->GetById($_GET['id']);
	$result = $answer->Eliminar($_SESSION['id_user']);
}

if(isset($_GET['accion']) && $_GET['accion'] == 'enable' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$answer->GetById($_GET['id']);
	$answer->habilitado = 1;
	$result = $answer->Modificar($_SESSION['id_user']);
}

if(isset($_GET['accion']) && $_GET['accion'] == 'disable' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$answer->GetById($_GET['id']);
	$answer->habilitado = 0;
	$result = $answer->Modificar($_SESSION['id_user']);
}



$answers = $answer->GetAll($_SESSION['id_user']);

//Llamar a la vista
require_once("views/answer/lstAnswer_view.phtml");
?>