<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");
//Llamar al modelo
require_once("models/Answer.class.php");

$answer = new Answer();
//Manejo de datos
if(isset($_POST['send']) and $_POST['send']=='ok')
{
	
	//Verificaciones
	if(empty($_POST['word']))
	{
		$error = 'Error, must enter a word.';
		require_once("views/answer/newAnswer_view.phtml");
		exit;
	}

	$answer->word = trim($_POST['word']);
	
	//Manejo de archivos y controles de extencion
	$tipo = $_POST['tipo'];

	switch ($tipo) {
    case 'text':
	    require_once('controls/messages/text.php');
        break;
    case 'imagen':
	 	require_once('controls/messages/image.php');	 
        break;
    case 'audio':
        require_once('controls/messages/audio.php');	
        break;
    case 'video':
        require_once('controls/messages/video.php');  
        break;    
	}
	
	if(!isset($error)){
		//Devuelve el id del sender con el que envio
		$answer->type = $tipo;
		$answer->message = $contenido;
		$answer->user = $_SESSION['id_user'];
		$result = $answer->Crear();		
	}

}

//Llamar a la vista
require_once("views/answer/newAnswer_view.phtml");
?>