<?php
define('__SELF_DIR__', dirname(dirname(__FILE__)));
include_once(__SELF_DIR__."../../lib/global.php");

//Llamar a al modelo
require_once("models/Encuesta.class.php");
require_once("models/Opcion.class.php");
require_once("models/Usuario.class.php");
require_once("models/Sender.class.php");
require_once("models/ContactoXEncuesta.class.php");

$encuesta = new Encuesta();
$sender = new Sender();
$usuario = new Usuario();
$option = new Opcion();
$contacto_x_encuesta = new ContactoXEncuesta();

$usuarios = $usuario->GetAll();
$usuario_selected = 0;

if($usuario->IsAdmin($_SESSION['id_user'])){
	$senders = $sender->GetAllArr();
}else{
	$senders = $sender->GetAllArr($_SESSION['id_user']);
}


//VIEW RESULT
if(isset($_GET["accion"]) && $_GET['accion'] == 'result' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$id =  $_GET['id'];
	$encuesta->GetById($id);
	
	$options = $option ->GetOptionsOfEncuesta($encuesta->id);
	
	require_once('views/encuesta/resultEncuesta_view.phtml');
	exit;
}

//VIEW EDIT
if(isset($_GET["accion"]) && $_GET['accion'] == 'edit' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$id =  $_GET['id'];
	$encuesta->GetById($id);
	
	$options = $option ->GetOptionsOfEncuesta($encuesta->id);
	
	require_once('views/encuesta/modEncuesta_view.phtml');
	exit;
}

//VIEW SHOW
if(isset($_GET["accion"]) && $_GET['accion'] == 'show' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$id =  $_GET['id'];
	$encuesta->GetById($id);
	
	$options = $option ->GetOptionsOfEncuesta($encuesta->id);
	
	require_once('views/encuesta/showEncuesta_view.phtml');
	exit;
}

//FINISH
if(isset($_GET["accion"]) && $_GET['accion'] == 'finish' && isset($_GET['id']) && is_numeric($_GET['id']))
{
	$encuesta->id =  $_GET['id'];
	$encuesta->GetById($encuesta->id);
	$sender->id = $encuesta->sender_id;
	
	$result = $encuesta->Finish();
	
	if($result['status']){
		$sender->Update_encuesta(0);
	}
	
	//para que aparezca el finalizado por diferencia de hora.
	sleep(2);
}

//EDIT CONTROL
if(isset($_POST["editar"]))
{
	$encuesta->id = $_POST["id"];
	
	$encuesta->msg_successfull = $_POST["msg_successfull"];
	$encuesta->msg_wrong_answer = $_POST["msg_wrong_answer"];
	
	$result = $encuesta->Modificar();
}


if(isset($_POST["search"]))
{
	if($usuario->IsAdmin($_SESSION['id_user'])){
		$usuario_selected = (isset($_POST['user'])? $_POST['user']:'0');
	}	
}

if($usuario->IsAdmin($_SESSION['id_user'])){
	$encuestas = $encuesta->GetAll($usuario_selected);
}else{
	$encuestas = $encuesta->GetAll($_SESSION['id_user']);
}

//Llamar a la vista
require_once("views/encuesta/lstEncuesta_view.phtml");
?>