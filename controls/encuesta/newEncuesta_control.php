<?php
require_once("models/Sender.class.php");
require_once("models/Encuesta.class.php");
require_once("models/Opcion.class.php");
require_once("models/ContactoXEncuesta.class.php");
require_once("models/Contact.class.php");
require_once("models/Chat.class.php");

$sender = new Sender();
$encuesta = new Encuesta();
$option = new Opcion();
$contacto_x_encuesta = new ContactoXEncuesta();
$contacto = new Contact();
$chat = new Chat();


if($user->IsAdmin($_SESSION['id_user'])){
	$senders = $sender->GetAllArr();
}else{
	$senders = $sender->GetAllArr($_SESSION['id_user']);
}

if(isset($_POST["aceptar"]))
{	
	$contenido 	   = '';
	$result['msg'] = ''; 
	
	$encuesta->ParseoDeArray($_POST);

	$contacto->sender_id = $_POST['sender_id'];
	$contacto->status = SUSCRIBE_STATUS;
	$contactos = $contacto->Search($_SESSION['id_user']);
	
	$encuesta->cant_total = count($contactos); 
	
	$sender->GetById($_POST['sender_id']);
	
	//Validacion
	if($sender->encuesta_id != 0){
		$result['status'] = false;
		$result['msg']    .= "The selected sender already have a survey assigned.<br />";
	}else{
		$result['status'] = true;
	}
	
	//OPCIONES
	if($_POST['option_value_1'] == '' || $_POST['option_value_2'] == '' || $_POST['option_text_1'] == '' || $_POST['option_text_2'] == ''){
		$result['status'] = false;
		$result['msg']    .= "The survey must have at least 2 options set.<br />";
	}else{
		$result['status'] = true;
	}
	
	if($result['status']){

			$result = $encuesta->Crear();
			
			if($result['status']){	
					
				$contenido .= $encuesta->question."\n";
				$contenido .= $encuesta->question_help."\n";		
				
				$option->encuesta_id = $encuesta->LastId();
				
				for($i=1;$i<=15;$i++){	
					
					$option->option_value = trim($_POST['option_value_'.$i]);
					$option->option_text  = trim($_POST['option_text_'.$i]);
					
					if($option->option_value != '' && $option->option_text != ''){
						$option->Crear();
						
						$contenido .= trim($option->option_value).' - '.trim($option->option_text)."\n";
					}
				}
				
				$contacto_x_encuesta->encuesta_id = $option->encuesta_id;
				
				$chat->message = $contenido;
				$chat->sender_id   = $_POST['sender_id'];
				$chat->type = 'text';
				$chat->mode = 'sent';
				
				foreach($contactos as $c){
					$contacto_x_encuesta->contact_id = $c->id;
					$result_bulk = $contacto_x_encuesta->CrearBulk();
					
					//CHAT
					$chat->contact_id = $c->id;			
					$result_bulk &= $chat->CrearBulk();		
				}
				
				$result_bulk &= $contacto_x_encuesta->Save();
				$result_bulk &= $chat->Save();
				
				
				//SET Encuesta
				$sender->Update_encuesta($option->encuesta_id);
			}
	}
	
}


require_once('views/encuesta/newEncuesta_view.phtml');
?>