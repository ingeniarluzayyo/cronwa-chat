<?php
	class Packages_x_User {
		
		public $tabla;
		public $id;
		public $package;
		public $user;
		public $fecha;
		
		public function __construct() {
			$this->tabla 			= 'packages_x_user';
			$this->id 				= '';
			$this->package			= '';
			$this->user				= '';
			$this->fecha				= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->package			= $arr['package'];
			$this->user				= $arr['user'];
			$this->fecha				= $arr['fecha'];

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {	
			$result = $this->ValidarDatos();
			if($result['state'])
			{		
				$arr = array(
					'package' 	=> $this->package,
					'user' 		=> $this->user,
					'fecha' 		=> DataBase::Cadena($this->fecha)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while buy the package.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The package has been request succesfully.";
			return $result;
		 }
		 
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
			
			return $result;
		 }
		 
		public function Modificar() {
			$result = $this->ValidarDatos();
			if($result['state'])
			{
				$arr = array(
					'package' 	=> $this->package,
					'user' 		=> $this->user,
					'fecha' 		=> DataBase::Cadena($this->fecha)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing the buy.";
					$result['state'] = false;
					return $result;
				}
				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The package has been added succesfully.";
			return $result;
		 }
		
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting the request.";
				$result['state'] = false;
				return $result;
			}
			
			$result['msg'] .= "The package has been delete succesfully.<br />";			
			return $result;			
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = ".$id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}		

		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla." ORDER BY fecha";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Packages_x_User();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
	
	}	

?>