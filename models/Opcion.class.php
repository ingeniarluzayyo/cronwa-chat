<?php
	class Opcion {
		
		public $tabla;
		public $id;		
		public $encuesta_id;
		public $option_text;
		public $option_value;
		public $cant;
		
		public $array_masivo=array();
		
		public function __construct() {
			$this->tabla 			= 'opciones';
			$this->id 				= 0;
			$this->encuesta_id		= 0;
			$this->option_text		= '';
			$this->option_value		= '';
			$this->cant				= 0;
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			if(array_key_exists('encuesta_id', $arr))
			{
			   $this->encuesta_id	= $arr['encuesta_id'];
			}			
			if(array_key_exists('option_text', $arr))
			{
			   $this->option_text	= $arr['option_text'];
			}
			if(array_key_exists('option_value', $arr))
			{
			   $this->option_value	= $arr['option_value'];
			}
			if(array_key_exists('cant', $arr))
			{
			   $this->cant	= $arr['cant'];
			}

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {		
			$result = $this->ValidarDatos();
						
			if($result['status'])
			{						
				$arr = array(
					'encuesta_id' 	=> $this->encuesta_id,					
					'option_text' 	=> DataBase::Cadena(str_replace("'","",$this->option_text)),
					'option_value'	=> DataBase::Cadena(str_replace("'","",$this->option_value)),					
					'cant'			=> 0
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a option.";
					$result['status'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			$result['msg'] .= "The option has been created succesfully.<br />";
			
			return $result;
		 }			
			
		public function ValidarDatos()
		 {
		 	$result['status'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->encuesta_id != 0 AND is_numeric($this->encuesta_id)))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The option must be assigned to a sender.<br />";
			}
			if(! ($this->option_text != ""))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The option must have a option text<br />";
			}					
						
			return $result;
		 }
		 
		public function Eliminar($id_user = 0) {
			$result['status'] = true;
			$result['msg'] 	 = "";
			
			if($id_user !=0){
				$where = "WHERE id = ".$this->id." AND encuesta_id IN (select id from encuestas where sender_id IN (SELECT id from senders where user=".$id_user."))" ;
			}else{
				$where = "WHERE id = ".$this->id;
			}
			
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a option.";
				$result['status'] = false;
				return $result;
			}
			
			$result['msg'] .= "The option has been deleted succesfully.<br />";			
			return $result;
		 }
		 
		 public function Update_cant() {

			$arr = array(
				'cant' 	=> $this->cant
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }


		public function GetById($id,$id_user = 0) {
			
			if($id_user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = ".$this->id." AND encuesta_id IN (select id from encuestas where sender_id IN (SELECT id from senders where user=".$id_user."))";
			}
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetByName($name,$encuesta) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE name = " .$name." and encuesta_id=".$encuesta." LIMIT 0,1";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function GetPercent($cant,$cant_total) {
			
			return ($cant * 100)/$cant_total;
		}	
		
		public function GetAll($id_user,$start = -1) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE encuesta_id IN (select id from encuestas where sender_id IN (SELECT id from senders where user=".$id_user.")) ORDER BY creation_date";			
			
			if($start!=-1)
			{
				$sql .= " LIMIT ".$start.",".CANTIDAD_ITEMS_PAGINA;	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Opcion();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}	
		
		public function GetOptionsOfEncuesta($encuesta_id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE encuesta_id = ".$encuesta_id;			
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Opcion();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}	
		
		public function ProcessWord($word,$encuesta_id)
		{
			$options = $this->GetOptionsOfEncuesta($encuesta_id);

			foreach($options as $o)
			{
				if($o->option_value == $word)
				{
					$o->cant = $o->cant + 1;
					$o->Update_cant();
					return $o->id;
				}
			}
			
			return 0;
		}					
	}	

?>