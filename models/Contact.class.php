<?php
	class Contact {
		
		public $tabla;
		public $id;
		public $sender_id;
		public $name;
		public $number;
		public $date_create;
		public $status;
		
		public $array_masivo=array();
		
		public function __construct() {
			$this->tabla 			= 'contacts';
			$this->id 				= 0;
			$this->sender_id		= '';
			$this->name				= '';
			$this->number			= '';
			$this->date_create		= '';
			$this->status			= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			if(array_key_exists('sender_id', $arr))
			{
			   $this->sender_id	= $arr['sender_id'];
			}
			if(array_key_exists('name', $arr))
			{
			   $this->name	= $arr['name'];
			}			
			if(array_key_exists('number', $arr))
			{
			   $this->number	= $arr['number'];
			}
			if(array_key_exists('date_create', $arr))
			{
			   $this->date_create	= $arr['date_create'];
			}
			if(array_key_exists('status', $arr))
			{
			   $this->status	= $arr['status'];
			}

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear($wa) {		
			$dt = new DateTime();
			
			$result = $this->ValidarDatos();
			
			if($this->GetByNumber($this->number,$this->sender_id))
			{
				if($this->status == UNSUSCRIBE_STATUS)
				{
					return $this->ReSuscribe();	
				}
				
				$result['status'] = false;
		 		$result['msg'] .= "The contact number has alredy exists in the database.<br />";
		 		return $result;				
			}
						
			if($result['status'])
			{						
				$arr = array(
					'sender_id' 	=> $this->sender_id,
					'name' 			=> DataBase::Cadena(str_replace("'","",$this->name)),
					'number' 		=> $this->number,
					'date_create' 	=> DataBase::Cadena($dt->format('Y-m-d H:i:s')),
					'status'		=> DataBase::Cadena(SUSCRIBE_STATUS)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a contact.";
					$result['status'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			$result['msg'] .= "The contact has been created succesfully.<br />";
			
			/**
			* delta Sync
			* incremental contacts sync. It will add one or more contacts 
			* to the existing contacts list. It should be used when you are 
			* interacting with a new contact
			**/
			$newContact = array($this->number);
			$wa->sendSync($newContact);
			
			
			return $result;
		 }		
		 
		 public function Baja() {
		 	$result = array();
		 	$result['msg'] = "";
					 	
		 	if(!$this->GetByNumber($this->number,$this->sender_id))
		 	{
				$result['msg'] .= "Error while unsuscribing a contact.";
				$result['status'] = false;
				echo "error";
				return $result;
			}
				
			$arr = array(
				'status'		=> DataBase::Cadena(UNSUSCRIBE_STATUS)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				$result['msg'] .= "Error while unsuscribing a contact.";
				$result['status'] = false;
				echo "error";
				return $result;
			}				
			
			
			$result['msg'] .= "The contact has been unsuscribed succesfully.<br />";
			return $result;
		 }
		 
		 public function ReSuscribe() {
		 	$result = array();
		 	$result['msg'] = "";		
		 	
		 	if(!$this->GetByNumber($this->number,$this->sender_id))
		 	{
				$result['msg'] .= "Error while suscribing a contact.";
				$result['status'] = false;
				return $result;
			}
				
			$arr = array(
				'status'		=> DataBase::Cadena(SUSCRIBE_STATUS)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				$result['msg'] .= "Error while unsuscribing a contact.";
				$result['status'] = false;
				return $result;
			}				
			
			
			$result['msg'] .= "The contact has been unsuscribed succesfully.<br />";
			return $result;
		 }
			
		public function ValidarDatos()
		 {
		 	$result['status'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->sender_id != 0 AND is_numeric($this->sender_id)))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The contact must be assigned to a sender.<br />";
			}
			if(! ($this->name != "" AND is_string($this->name)))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The contact must have a name<br />";
			}
			if(! ($this->number != ""))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The contact must have a number<br />";
			}					
						
			return $result;
		 }
		 
		public function Eliminar($id_user = 0) {
			$result['status'] = true;
			$result['msg'] 	 = "";
			
			if($id_user !=0){
				$where = "WHERE id = ".$this->id." AND sender_id IN (select id from senders where user=".$id_user.")" ;
			}else{
				$where = "WHERE id = ".$this->id;
			}
			
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a contact.";
				$result['status'] = false;
				return $result;
			}
			
			$result['msg'] .= "The contact has been deleted succesfully.<br />";			
			return $result;
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id,$id_user = 0) {
			
			if($id_user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id." AND sender_id IN (select id from senders where user=".$id_user.")";
			}
			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetByNumber($number,$sender) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE number = " .$number." and sender_id=".$sender." LIMIT 0,1";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetAmount($sender) {
			$sql  = "SELECT count(1) as cantidad FROM ".$this->tabla." WHERE status='".SUSCRIBE_STATUS."' and sender_id=".$sender;			
						
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
			
						
			return $arr['cantidad'];
		}
		
		public function GetAll($id_user,$start = -1) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE sender_id IN (select id from senders where user=".$id_user.") ORDER BY name";			
			
			if($start!=-1)
			{
				$sql .= " LIMIT ".$start.",".CANTIDAD_ITEMS_PAGINA;	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Contact();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}		
		
		public function Search($order,$id_user=0) {
			
			if($id_user !=0){
				$sql  = "SELECT con.id,name,number,con.sender_id,date_create,status FROM ".$this->tabla." as con INNER JOIN chat as c on c.contact_id=con.id WHERE con.sender_id = ".$this->sender_id." AND con.sender_id IN (select id from senders where user=".$id_user.") ";			
			}else{
				$sql  = "SELECT con.id,name,number,con.sender_id,date_create,status FROM ".$this->tabla." as con INNER JOIN chat as c on c.contact_id=con.id WHERE con.sender_id = ".$this->sender_id;			
			}
			
			if($this->name != ""){
			  $sql .= " AND name like '%".$this->name."%'";
			}
			
			if($this->number != ""){
				$sql .= " AND number like '%".$this->number."%'";
			}
			
			if($this->status != ""){
				$sql .= " AND status ='".$this->status."'";
			}
			
			if($order == 'ordername'){
				$sql .= " GROUP BY con.id ORDER BY name";
			}else{
				$sql .= " GROUP BY con.id ORDER BY c.datetime desc";
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}						
			
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Contact();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}	

		
		public function GetGroupName() {
			$sql  = "SELECT name FROM ".$this->tabla_groups." WHERE id = " . $this->sender_id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return "";
			}

			return $arr['name'];			
		}	
		
		public function ProcessWord($body,$sender,$whatsapp_port)
		{
			$settings_user = new Usuario();
			$settings_user->GetById($sender->user);	
							
			$this->GetByNumber($this->number,$sender->id);		
			
			$word =  strtolower(strip_tags(trim($body)));
			
			if($this->id != 0)
			{
				$result = "";

				//Control de encuesta
				$sender->GetById($sender->id);//Actualizo los datos del sender
				
				if($sender->encuesta_id != 0)
				{
					$encuesta = new Encuesta();
					$encuesta->GetById($sender->encuesta_id);
					
					//Guardo el mensaje recibido
					$chat = new Chat();
					$chat->sender_id  = $sender->id;
					$chat->contact_id = $this->id;
					$chat->message 	  = $word;
					$chat->mode       = 'received';
					$chat->type       = 'text';
					$chat->Crear();
					
					
					$result = $encuesta->ProcessWord($word,$sender,$this,$whatsapp_port);
					return $result;
				}
			}
			
			switch($word)
			{
				case strtolower($settings_user->alta_word): {
					$result = $this->Crear($whatsapp_port);
					break;
				}
				
				case strtolower($settings_user->baja_word): {
					$result = $this->Baja();
					break;	
				}
			}
						
			$this->GetByNumber($this->number,$sender->id);
			
			if($this->id != 0)
			{
				//Guardo el mensaje recibido
				$chat = new Chat();
				$chat->sender_id  = $sender->id;
				$chat->contact_id = $this->id;
				$chat->message 	  = trim($body);
				$chat->mode       = 'received';
				$chat->type       = 'text';
				$chat->Crear();
				
				if($settings_user->modulo_answer){
					$answer = new Answer();
					$answer->ProcessWord($word,$sender,$this,$whatsapp_port);
				}
				
			}
		}			
	}	

?>