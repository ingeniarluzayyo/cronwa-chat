<?php
	class HashMedia {
		
		public $id;
		public $hash;
		public $fecha;
		public $tabla;
		
		public function __construct() {
			$this->id 				= '';
			$this->hash			= '';
			$this->fecha 		= '';
			$this->tabla 			= 'hash_media';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}			
			$this->hash			= $arr['hash'];
			$this->fecha 		= $arr['fecha'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		public function Crear() {
			$dt = new DateTime();			
			$arr = array(
				'hash' 			=> DataBase::Cadena($this->hash),
				'fecha' 		=> DataBase::Cadena($dt->format('Y-m-d'))								
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {			
				return false;
			}				
						
			return true;
		 }
		 
		public function Modificar() {
			$dt = new DateTime();			
			$arr = array(					
				'fecha' 		=> DataBase::Cadena($dt->format('Y-m-d'))
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
				return false;
			}			
			return true;
				
		 }	
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting an hash.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The hash has been deleted succesfully.<br />";			
			return $result;	
		 }


		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
		
		public function GetByHash($hash) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE hash = '" . $hash."'";			
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}			
		
		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla;			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Usuarios = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newUser = new HashMedia();
				$newUser->ParseoDeArray($row);
				array_push($Usuarios,$newUser);
			}
						
			return $Usuarios;
		}
		
	}	

?>