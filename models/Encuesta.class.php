<?php
	class Encuesta {
		
		public $tabla;
		public $id;		
		public $name;
		public $sender_id;
		public $question;
		public $question_help;
		public $msg_wrong_answer;
		public $msg_successfull;
		public $creation_date;
		public $finish_date;
		public $cant_total;
		
		public $array_masivo=array();
		
		public function __construct() {
			$this->tabla 			= 'encuestas';
			$this->id 				= 0;
			$this->sender_id		= 0;
			$this->name				= '';
			$this->question			= '';
			$this->question_help	= '';
			$this->msg_wrong_answer	= '';
			$this->msg_successfull	= '';
			$this->creation_date	= date("Y-m-d H:i:s");
			$this->finish_date		= '';
			$this->cant_total		= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			if(array_key_exists('sender_id', $arr))
			{
			   $this->sender_id	= $arr['sender_id'];
			}
			if(array_key_exists('name', $arr))
			{
			   $this->name	= $arr['name'];
			}			
			if(array_key_exists('question', $arr))
			{
			   $this->question	= $arr['question'];
			}
			if(array_key_exists('question_help', $arr))
			{
			   $this->question_help	= $arr['question_help'];
			}
			if(array_key_exists('msg_wrong_answer', $arr))
			{
			   $this->msg_wrong_answer	= $arr['msg_wrong_answer'];
			}
			if(array_key_exists('msg_successfull', $arr))
			{
			   $this->msg_successfull	= $arr['msg_successfull'];
			}
			if(array_key_exists('creation_date', $arr))
			{
			   $this->creation_date	= $arr['creation_date'];
			}
			if(array_key_exists('finish_date', $arr))
			{
			   $this->finish_date	= $arr['finish_date'];
			}
			if(array_key_exists('cant_total', $arr))
			{
			   $this->cant_total	= $arr['cant_total'];
			}

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {		
			$dt = new DateTime();
			
			$result = $this->ValidarDatos();
			
			if($this->GetByName($this->name,$this->sender_id))
			{			
				$result['status'] = false;
		 		$result['msg'] .= "The survey name has alredy exists in the database.<br />";
		 		return $result;				
			}
						
			if($result['status'])
			{						
				$arr = array(
					'sender_id' 			=> $this->sender_id,
					'name' 					=> DataBase::Cadena($this->name),
					'question' 				=> DataBase::Cadena($this->question),
					'question_help' 		=> DataBase::Cadena($this->question_help),
					'msg_wrong_answer' 		=> DataBase::Cadena($this->msg_wrong_answer),
					'msg_successfull' 		=> DataBase::Cadena($this->msg_successfull),
					'creation_date' 		=> DataBase::Cadena($dt->format('Y-m-d H:i:s')),
					'finish_date' 			=> DataBase::Cadena($this->finish_date),
					'cant_total'			=> DataBase::Cadena($this->cant_total)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a survey.";
					$result['status'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The survey has been created succesfully.<br />";
			
			return $result;
		 }			
			
		public function ValidarDatos()
		 {
		 	$result['status'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->sender_id != 0 AND is_numeric($this->sender_id)))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey must be assigned to a sender.<br />";
			}
			if(! ($this->name != "" AND is_string($this->name)))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey must have a name<br />";
			}
			if(! ($this->question != ""))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey must have a question<br />";
			}		
			/*if(! ($this->question_help != ""))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey must have a question help<br />";
			}*/
			if(! ($this->msg_wrong_answer != ""))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey must have a message wrong answer<br />";
			}	
			if(! ($this->finish_date != ""))
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey must have a finish date<br />";
			}
			if($this->finish_date < $this->creation_date)
		 	{
				$result['status'] = false;
		 		$result['msg'] .= "The survey can not have a lower finish date to the date of creation<br />";
			}	
						
			return $result;
		 }
		 
		public function Eliminar($id_user = 0) {
			$result['status'] = true;
			$result['msg'] 	 = "";
			
			if($id_user !=0){
				$where = "WHERE id = ".$this->id." AND sender_id IN (select id from senders where user=".$id_user.")" ;
			}else{
				$where = "WHERE id = ".$this->id;
			}
			
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a survey.";
				$result['status'] = false;
				return $result;
			}
			
			$result['msg'] .= "The survey has been deleted succesfully.<br />";			
			return $result;
		 }
		 
		 public function Modificar() {
			$result['status'] = true;
			$result['msg'] 	 = "";
				
			if($result['status'])
			{
				$arr = array(
					'msg_successfull' 				=> DataBase::Cadena($this->msg_successfull),
					'msg_wrong_answer' 				=> DataBase::Cadena($this->msg_wrong_answer)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
						$result['msg'] .= "Error while editing a survey.";
						$result['status'] = false;
						return $result;								
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The survey has been edited succesfully.<br />";
			return $result;			
		 }
		 
		  public function Finish() {
			$result['status'] = true;
			$result['msg'] 	 = "";
				
			if($result['status'])
			{
				$arr = array(
					'finish_date' 				=> DataBase::Cadena(date("Y-m-d H:i:s"))
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
						$result['msg'] .= "Error while editing a survey.";
						$result['status'] = false;
						return $result;								
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The survey has been finished succesfully.<br />";
			return $result;			
		 }


		public function LastId() {
			$sql  = "SELECT max(id) as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}
				
		
			return $arr["id"];
		}

		public function GetById($id,$id_user = 0) {
			
			if($id_user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id." AND sender_id IN (select id from senders where user=".$id_user.")";
			}

			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
			
			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetByName($name,$sender) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE name = '" .$name."' and sender_id=".$sender." LIMIT 0,1";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetAll($id_user,$start = -1) {
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE 1=1 ";			
			
			if($id_user != 0)
			{
				$sql .= " AND sender_id IN (select id from senders where user=".$id_user.")";
			}
			
			$sql .=  " ORDER BY creation_date";
			
			if($start!=-1)
			{
				$sql .= " LIMIT ".$start.",".CANTIDAD_ITEMS_PAGINA;	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Encuesta();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function ProcessWord($word,$sender,$contact,$whatsapp_port)
		{						
			$result = "";
			$message = new Messages();
			$option = new Opcion();
			$contacto_x_encuesta = new ContactoXEncuesta();
					
			$contacto_x_encuesta->encuesta_id = $this->id;
			$contacto_x_encuesta->contact_id = $contact->id;
			if(!$contacto_x_encuesta->ControlDeVotacion())
			{
				return false;
			}
			
			$result = $option->ProcessWord($word,$this->id);//Retorna el id de la opcion votada
			
			if(!$contacto_x_encuesta->GetByContactoYEncuesta($this->id,$contact->id))
			{
				return false;
			}
			
			if($result > 0)
			{
				$contacto_x_encuesta->option_id = $result;
				$contacto_x_encuesta->Update_option();
				
				if($this->msg_successfull != ''){
					$msg = $this->msg_successfull;
					$result = $message->Send($sender->user,$contact,'text',$this->msg_successfull,$sender,$whatsapp_port);
				}
			}else{
				if($this->msg_wrong_answer){
					$msg = $this->msg_wrong_answer;
					$result = $message->Send($sender->user,$contact,'text',$this->msg_wrong_answer,$sender,$whatsapp_port);
				}				
			}
			
			//Guardo el mensaje enviado
			if($result && isset($msg)){
				$chat = new Chat();
				$chat->sender_id  = $sender->id;
				$chat->contact_id = $contact->id;
				$chat->message 	  = addslashes($msg);
				$chat->mode       = 'sent';
				$chat->sent       = '1';
				$chat->type       = 'text';
				$chat->Crear();
			}
			
			return $result;
		}					
	}	

?>