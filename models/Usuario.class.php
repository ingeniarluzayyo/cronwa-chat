<?php
	class Usuario {
		
		public $id;
		public $username;
		public $password;
		public $email;
		public $telefono;
		public $user_type;
		public $credits;
		public $alta_word;
		public $baja_word;
		public $default_answer;
		
		public $modulo_answer;
		public $modulo_chat;
		public $modulo_survey;
		public $modulo_campaign;
		
		public $habilitado;
		public $tabla;
		
		public function __construct() {
			$this->id 				= '';
			$this->username			= '';
			$this->password 		= '';
			$this->email 			= '';
			$this->telefono 		= '';
			$this->user_type 		= '';
			$this->credits	 		= '';
			$this->alta_word		= 'Suscribe';
			$this->baja_word		= 'Unsuscribe';
			$this->default_answer	= 'Invalid command';
			
			$this->modulo_answer	= '0';
			$this->modulo_chat		= '0';
			$this->modulo_survey	= '0';
			$this->modulo_campaign	= '0';
			
			$this->habilitado 		= '';	
			$this->tabla 			= 'usuarios';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 				= $arr['id'];
			}
			if(array_key_exists('username', $arr))
			{			
				$this->username			= $arr['username'];
			}
			if(array_key_exists('password', $arr))
			{
				$this->password 		= $arr['password'];
			}
			if(array_key_exists('email', $arr))
			{
				$this->email 			= $arr['email'];
			}
			if(array_key_exists('telefono', $arr))
			{
				$this->telefono 		= $arr['telefono'];
			}
			if(array_key_exists('credits', $arr))
			{
				$this->credits			= $arr['credits'];
			}
			if(array_key_exists('user_type', $arr))
			{
				$this->user_type		= $arr['user_type'];
			}
			if(array_key_exists('alta_word', $arr))
			{
				$this->alta_word		= $arr['alta_word'];
			}
			if(array_key_exists('baja_word', $arr))
			{	
				$this->baja_word		= $arr['baja_word'];
			}
			if(array_key_exists('default_answer', $arr))
			{
				$this->default_answer 	= $arr['default_answer'];
			}
			if(array_key_exists('habilitado', $arr))
			{
				$this->habilitado 		= $arr['habilitado'];
			}
			
			if(array_key_exists('modulo_answer', $arr))
			{
				$this->modulo_answer		= $arr['modulo_answer'];
			}
			if(array_key_exists('modulo_chat', $arr))
			{
				$this->modulo_chat		= $arr['modulo_chat'];
			}
			if(array_key_exists('modulo_survey', $arr))
			{	
				$this->modulo_survey		= $arr['modulo_survey'];
			}
			if(array_key_exists('modulo_campaign', $arr))
			{
				$this->modulo_campaign 	= $arr['modulo_campaign'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		public function Crear() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{							
				$arr = array(
					'username' 			=> DataBase::Cadena($this->username),
					'password' 			=> DataBase::Cadena(md5($this->password)),				
					'email' 			=> DataBase::Cadena($this->email),
					'telefono' 			=> DataBase::Cadena($this->telefono),
					'alta_word' 		=> DataBase::Cadena($this->alta_word),
					'baja_word' 		=> DataBase::Cadena($this->baja_word),
					'default_answer' 	=> DataBase::Cadena($this->default_answer),
					'habilitado' 		=> DataBase::Cadena($this->habilitado),
					'modulo_answer' 	=> DataBase::Cadena($this->modulo_answer),
					'modulo_chat' 		=> DataBase::Cadena($this->modulo_chat),
					'modulo_survey' 	=> DataBase::Cadena($this->modulo_survey),
					'modulo_campaign' 	=> DataBase::Cadena($this->modulo_campaign)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating an user.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The user has been created succesfully.<br />";
			return $result;
		 }
		 
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				//Para modificar los creditos usar la funcion AddCredits()
				$arr = array(
					'username' 			=> DataBase::Cadena($this->username),
					'password' 			=> DataBase::Cadena(md5($this->password)),
					'email' 			=> DataBase::Cadena($this->email),
					'telefono' 			=> DataBase::Cadena($this->telefono),
					'habilitado' 		=> DataBase::Cadena($this->habilitado),
					'modulo_answer' 	=> DataBase::Cadena($this->modulo_answer),
					'modulo_chat' 		=> DataBase::Cadena($this->modulo_chat),
					'modulo_survey' 	=> DataBase::Cadena($this->modulo_survey),
					'modulo_campaign' 	=> DataBase::Cadena($this->modulo_campaign)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing an user.";
					$result['state'] = false;
					return $result;
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The user has been edited succesfully.<br />";
			return $result;
				
		 }
		 
		 public function Modificar_user_settings() {
			$result = $this->ValidarDatos_user_settings();	
			if($result['state'])
			{
				$arr = array(
					'alta_word'			=> DataBase::Cadena(trim($this->alta_word)),
					'baja_word' 		=> DataBase::Cadena(trim($this->baja_word)),
					'default_answer' 	=> DataBase::Cadena(trim($this->default_answer))
				);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing the user settings.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The user settings has been edited succesfully.<br />";
			return $result;
		 }
		 
		 public function ValidarDatos_user_settings()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if($this->alta_word == "")
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The 'Suscribe word' field is required.<br />";
			}
			if($this->baja_word == "")
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The 'Unsuscribe word' field is required.<br />";
			}
			
			return $result;
		 }
		 
		public function func() {
	        $debug = debug_backtrace();
	        return $debug[2]['function'];
	    } 
	 
		 
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->username != "" AND is_string($this->username)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on username.<br />";
			}
			
			if (self::func() != 'Modificar'){
				if($this->password == "")
			 	{
					$result['state'] = false;
			 		$result['msg'] .= "You must put a password.<br />";
				}
			}
			
			return $result;
		 }
		 
		 public function AddCredits($credits) {	
		 	$result['state'] = true;
			$result['msg'] 	 = "";
			
		 	if(is_numeric($credits))
		 	{								
				$arr = array(
					'credits' 		=> ($this->credits + $credits)			
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while adding credits.";
					$result['state'] = false;
					return $result;
				}
				$result['msg'] .= "The credits has been added succesfully.<br />";			
				return $result;
			}
				$result['msg'] .= "Invalid character.";
				$result['state'] = false;
				return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting an users.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The user has been deleted succesfully.<br />";			
			return $result;	
		 }


		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
		
		public function IsAdmin($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE user_type = 'admin' AND id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
		
			return true;
		}
		
		public function GetByUsername($username) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE username = '".$username."'";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}
						
			$this->ParseoDeArray($arr);
		
			return true;
		}
		
		public function GetUsername($id_user) { 
			$sql  = "SELECT username FROM ".$this->tabla." WHERE id = " . $id_user;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}						
			return $arr['username'];
		}
		
		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla;			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Usuarios = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newUser = new Usuario();
				$newUser->ParseoDeArray($row);
				array_push($Usuarios,$newUser);
			}
						
			return $Usuarios;
		}
		
		public function GetArrayAll() {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE user_type != 'admin'";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			
			$Contenidos = array();						
			
			while($row = mysqli_fetch_array($arr))
			{				
				$Contenidos[$row["id"]] = $row["username"];
			}
						
			return $Contenidos;
		}
		
	}	

?>