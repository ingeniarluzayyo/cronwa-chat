<?php
class Messages{
		
		public function __construct() {
			
		}
		
		public function Send($user,$contact,$tipo,$contenido,$sender = "",$whatsapp) {
			if($sender == "")
			{
				echo "The sender is not setted";
				return false;
			}
			
			if($sender->id == 0){return 0;}											

			$usuario = new Usuario();
			$usuario->GetById($user);

			try{
				if($usuario->credits > 0){
					if($tipo == 'text')
					{
						return $this->SendText($usuario,$sender,$contenido,$contact,$whatsapp);
					}else{
						return $this->SendMultimedia($usuario,$sender,$contenido,$contact,$tipo,$whatsapp);
					}
				}
			}catch(Exception $e){				
				return false;
			}
			 
		}
		
		private function SendText($user,$sender,$content,$contact,$wa)
		{					
			$replace= array('�','�','�','�', '�', '�', '�', '�', '�','�', '�', '�', '�', '�', '�');
			$find = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
			$content = str_ireplace($find,$replace,$content);
			if(strstr($content,'{name}') != false){$contenido = str_replace('{name}',$contact->name,utf8_encode($content));}
		 
			
			if($wa->sendMessage($contact->number, utf8_encode($content))!= "")
			{
				//$wa->pollMessage();
				
				//RESTO CREDITO
				$user->AddCredits(-1);	
					
				return true;
			}
			
			return false;
		}
		
		private function SendMultimedia($user,$sender,$content,$contact,$type,$wa)
		{
			$hashMedia = new HashMedia();
						
			//reemplazo caracteres
			$replace= array('�','�','�','�', '�', '�', '�', '�', '�','�', '�', '�', '�', '�', '�');
			$find = array('&#8226;','&ordm;','&ordm;','&aacute;', '&eacute;', '&iacute;', '&oacute;', '&uacute;', '&ntilde;','&Aacute;', '&Eacute;', '&Iacute;', '&Oacute;', '&Uacute;', '&Ntilde;');
			
			$content = str_ireplace($find,$replace,$content);
			
			$array_contenido = explode(';',$content);
			
			if(count($array_contenido) == 2)
			{
				$contenido 	= $array_contenido[0];
				$caption 	= $array_contenido[1];	
			}else{
				$contenido 	= $array_contenido[0];
				$caption = '';
			}
			
			//por si tiene acentos y eso con ; 
			if(count($array_contenido)>2){
				for($i=2;$i<count($array_contenido);$i++){
					$caption .= ';'.$array_contenido[$i];
				}
			}
						
			$filehash = base64_encode(hash_file("sha256", $contenido, true));
			$filesize = filesize($contenido);

			$estaElHash = $hashMedia->GetByHash($filehash);															 
			
			if($estaElHash)
			{				
				$dtHash = new DateTime($hashMedia->fecha);
				$dtNow = new DateTime("now");
				$diferencia = $dtNow->diff($dtHash);	
			}	
								
			if($estaElHash && !$diferencia->d < 1)
			{						
				$hashMedia->Modificar();	
			}
			
			$result = "";
			switch($type)
			{
				case "image":{
					$result = $wa->sendMessageImage($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'',utf8_encode($caption));
					break;
				}
				case "audio":{
					$result = $wa->sendMessageAudio($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'');
					break;
				}
				case "video":{
					$result = $wa->sendMessageVideo($contact->number, $contenido,false,$filesize,($estaElHash)?$filehash:'',utf8_encode($caption));
					break;
				}
			}
			
			if($result != "")
			{
				//$wa->pollMessage();	
				
				if($estaElHash)
				{
					$hashMedia->hash = $filehash;
					$hashMedia->Crear();
				}	
				
				//RESTO CREDITO
				$user->AddCredits(-1);
											
				return true;		
			}											
			return false;	 					 
		}
}
?>