<?php
class Campaign{
		
		public $tabla;
		public $id;
		public $content;
		public $date;
		public $type;
		public $sender_id;

		
		private $dt;
		
		public function __construct() {
			$this->tabla 			= 'campaign';
			$this->id 				= '';			
			$this->content  		= '';
			$this->date 			= '';
			$this->type 			= '';
			$this->sender_id 		= '';	
			
			
			$this->dt = new DateTime();
		}
		
			
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->content 			= $arr['content'];									
			$this->date 			= $arr['date'];	
			$this->type 			= $arr['type'];
			$this->sender_id 		= $arr['sender_id'];

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}
		
		public function Crear() {			
			$arr = array(
				'content' 				=> DataBase::Cadena($this->content),
				'date' 				    => DataBase::Cadena($this->dt->format('Y-m-d H:i:s')),
				'type' 				    => DataBase::Cadena($this->type),
				'sender_id' 			=> DataBase::Cadena($this->sender_id)
				);
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				return false;
			}
			
			return true;						
		}	

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function Eliminar() {						
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {				
				return false;
			}						
						
			return true;			
		 }
		 		
		public function GetQueues($id_user = 0) {
			if($id_user != 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE sender_id IN (select id from senders where user=".$id_user.") ORDER BY date";
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE ORDER BY date";
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Campaign();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
				
		public function LastId() {
			$sql  = "SELECT max(id) as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}
		
		public function GetStatus($sent,$total) {
			if($sent == $total){
				return 'complete';
			}
			if($sent == 0){
				return 'waiting';
			}
			if($sent < $total){
				return 'in progress';
			}

			return 'error';
		}
		
		public function Search($id_user,$from,$to) {
			$sql  = "SELECT * FROM ".$this->tabla;
			$where = " WHERE 1=1";
			
			if($id_user != 0)
			{
				$where .= " AND sender_id IN (select id from senders where user=".$id_user.")";
			}
			if($from != '')
			{
				$where .= " AND date >= '".$from."'";
			}
			if($to != '')
			{
				$where .= " AND date <= '".$to."'";
			}
			$sql .= $where . " ORDER BY date DESC";
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Messages = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newMsg = new Campaign();
				$newMsg->ParseoDeArray($row);
				array_push($Messages,$newMsg);
			}
						
			return $Messages;
		}
		
}
?>