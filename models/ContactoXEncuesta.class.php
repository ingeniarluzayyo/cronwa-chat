<?php
class ContactoXEncuesta {
		
		public $tabla;
		public $id;
		public $contact_id;
		public $encuesta_id;
		public $option_id;
		
		private $array_masivo;
		
		public function __construct() {
			$this->tabla 					= 'contacto_x_encuesta';
			$this->id 						= '';
			$this->contacto_id				= '';
			$this->encuesta_id 				= '';
			$this->option_id 				= 0;
			
			$this->array_masivo             = array();
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->contact_id			= $arr['contact_id'];
			$this->encuesta_id 		= $arr['encuesta_id'];			
			if(array_key_exists('option_id', $arr))
			{
				$this->option_id 			= $arr['option_id'];
			}

		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {			
			$dt = new DateTime();	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'contact_id' 			=> DataBase::Cadena(trim($this->contact_id)),
					'option_id' 			=> DataBase::Cadena($this->option_id),
					'encuesta_id' 			=> DataBase::Cadena($this->encuesta_id)
					);
					if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a contact for survey.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The contact for survey has been created succesfully.<br />";
			return $result;
		}
		
		public function CrearBulk() {
			
			$arr = array(
					'contact_id' 			=> DataBase::Cadena(trim($this->contact_id)),
					'option_id' 			=> DataBase::Cadena($this->option_id),
					'encuesta_id' 			=> DataBase::Cadena($this->encuesta_id)
					);
			array_push($this->array_masivo,$arr);
			return true;
		}	
		
		public function Save()
		{
			if(!Database::getInstance()->insert_bulk($this->array_masivo, $this->tabla)) {
				return false;
			}
			
			return true;
		}
		
		public function Update_option() {

			$arr = array(
				'option_id' 	=> $this->option_id
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		  
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 					
			return $result;
		 }
		 
			
		public function GetById($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}
		
		public function GetNonresponders($encuesta_id = 0) {
			if($encuesta_id != 0)
			{
				$this->encuesta_id = $encuesta_id;
			}
			
			$sql  = "SELECT count(id) as cant FROM ".$this->tabla." WHERE encuesta_id = " . $this->encuesta_id." AND option_id = 0";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}	
		
			return $arr['cant'];
		}	
		
		public function GetByContactoYEncuesta($encuesta,$contacto) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE encuesta_id = " . $encuesta." AND contact_id = ".$contacto;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}		
		
		public function GetAll($user = 0) {
			$sql  = "SELECT * FROM ".$this->tabla."";	
					
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new ContactoXEncuesta();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function ControlDeVotacion()
		{
			$sql  = "SELECT * FROM ".$this->tabla." WHERE encuesta_id = " . $this->encuesta_id." AND contact_id = ".$this->contact_id." AND option_id = 0";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}			
		
			return true;			
		}		
}
?>