<?php
class Answer {
		
		public $tabla;
		public $id;
		public $word;
		public $message;
		public $type;
		public $user;
		public $habilitado;
		
		public function __construct() {
			$this->tabla 					= 'answer';
			$this->id 						= '';
			$this->word						= '';
			$this->message 					= '';
			$this->type 					= '';
			$this->user 					= '';
			$this->habilitado 				= '1';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->word			= $arr['word'];
			$this->message 		= $arr['message'];
			
			if(array_key_exists('type', $arr))
			{
				$this->type 			= $arr['type'];
			}
			if(array_key_exists('user', $arr))
			{
				$this->user 			= $arr['user'];
			}
			if(array_key_exists('habilitado', $arr))
			{
				$this->habilitado		= $arr['habilitado'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {			
			$dt = new DateTime();	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'word' 					=> DataBase::Cadena(trim($this->word)),
					'type' 					=> DataBase::Cadena($this->type),
					'message' 				=> DataBase::Cadena($this->message),
					'user' 					=> $this->user,
					'habilitado' 			=> DataBase::Cadena($this->habilitado)
					);
					if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a Answer.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The Answer has been created succesfully.<br />";
			return $result;
		}
		
		public function Modificar($id_user = 0) {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'habilitado' 	=> DataBase::Cadena($this->habilitado)
					);
				if($id_user !=0){
					$where = "WHERE id = ".$this->id." AND user =".$id_user;
				}else{
					$where = "WHERE id = ".$this->id;
				}
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
						$result['msg'] .= "Error while editing a Answer.";
						$result['state'] = false;
						return $result;								
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The Answer has been edited succesfully.<br />";
			return $result;			
		 }
		 
		 
		 
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	
					
			return $result;
		 }
		 
		 public function Eliminar($id_user = 0) {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
			if($id_user !=0){
				$where = "WHERE id = ".$this->id." AND user =".$id_user;
			}else{
				$where = "WHERE id = ".$this->id;
			}
			
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a Answer.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The Answer has been deleted succesfully.<br />";			
			return $result;	
		 }
			

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}		

		public function GetById($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetByUser($user = 0) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE user = ".$this->user;	
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Answer();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		
		public function GetAll($user = 0) {
			if($user == 0){
				$sql  = "SELECT * FROM ".$this->tabla."";	
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE user=".$user;	
			}
					
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Answer();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function ProcessWord($body,$sender,$contact,$whatsapp_port)
		{
			
			$words_of_user = $this->GetAll($sender->user);			
			
			$word =  strtolower(strip_tags(trim($body)));
			$result = "";
			$message = new Messages();
			
			foreach($words_of_user as $w)
			{
				if($word == strtolower($w->word))
				{

					$result = $message->Send($sender->user,$contact,$w->type,$w->message,$sender,$whatsapp_port);

					//Guardo el mensaje enviado
					if($result){
						$chat = new Chat();
						$chat->sender_id  = $sender->id;
						$chat->contact_id = $contact->id;
						$chat->message 	  = addslashes($w->message);
						$chat->mode       = 'sent';
						$chat->sent       = '1';
						$chat->type       = $w->type;
						$chat->Crear();
					}
					
					return $result;
				}			
			}
			
			$settings_user = new Usuario();
			$settings_user->GetById($sender->user);
			
			if($settings_user->default_answer != "" && $word != $settings_user->alta_word && $word != $settings_user->baja_word)
			{
				$result = $message->Send($sender->user,$contact,'text',$settings_user->default_answer,$sender,$whatsapp_port);	
				
				if($result){
					$chat = new Chat();
					$chat->sender_id  = $sender->id;
					$chat->contact_id = $contact->id;
					$chat->message 	  = $settings_user->default_answer;
					$chat->mode       = 'sent';
					$chat->sent       = '1';
					$chat->type       = 'text';
					$chat->Crear();
				}
					
				return $result;
			}	
			
			exit;
			return $result;
		}		
}
?>