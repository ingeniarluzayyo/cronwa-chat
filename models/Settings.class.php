<?php
	class Settings {
		
		public $tabla;
		public $id;
		public $site_name;
		public $currency;	
		public $timezone;		
		public $hora_inicio;	
		public $hora_fin;
		public $sleep_time_between_msg;	
		
		public function __construct() {
			$this->tabla 			= 'settings';			
			$this->id 				= '';
			$this->site_name		= '';
			$this->currency			= '';	
			$this->timezone			= '';			
			$this->whatsapp_version	= '';
			$this->hora_inicio		= '';
			$this->hora_fin			= '';
			$this->sleep_time_between_msg = 1;
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->site_name		= $arr['site_name'];
			$this->currency			= $arr['currency'];
			$this->timezone	= $arr['timezone'];			
			$this->whatsapp_version	= $arr['whatsapp_version'];
			if(array_key_exists('hora_inicio', $arr))
			{
				$this->hora_inicio 			= $arr['hora_inicio'];
			}
			if(array_key_exists('hora_fin', $arr))
			{
				$this->hora_fin 			= $arr['hora_fin'];
			}
			if(array_key_exists('sleep_time_between_msg', $arr))
			{
				$this->sleep_time_between_msg 			= $arr['sleep_time_between_msg'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{		
				$arr = array(
					'site_name' 		=> DataBase::Cadena($this->site_name),
					'currency' 			=> DataBase::Cadena($this->currency),
					'timezone' 			=> DataBase::Cadena($this->timezone),
					'whatsapp_version' 	=> DataBase::Cadena($this->whatsapp_version),										
					'hora_inicio' 		=> DataBase::Cadena($this->hora_inicio),
					'hora_fin' 				=> DataBase::Cadena($this->hora_fin),
					'sleep_time_between_msg' 		=> DataBase::Cadena($this->sleep_time_between_msg)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a contact.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			$result['msg'] .= "The settings has been created succesfully.<br />";
			
			return $result;
		 }
		 
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'site_name'			=> DataBase::Cadena($this->site_name),
					'currency' 			=> DataBase::Cadena($this->currency),
					'timezone' 			=> DataBase::Cadena($this->timezone),
					'whatsapp_version' 	=> DataBase::Cadena($this->whatsapp_version),
					'hora_inicio' 		=> DataBase::Cadena($this->hora_inicio),
					'hora_fin' 		=> DataBase::Cadena($this->hora_fin),
					'sleep_time_between_msg' 		=> DataBase::Cadena($this->sleep_time_between_msg)
				);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing the settings.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The settings has been edited succesfully.<br />";
			return $result;
		 }
		 
		public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->site_name != ""))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The settings must have a site name.<br />";
			}
			if(! ($this->currency != "" AND is_string($this->currency)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The settings must have a currency.<br />";
			}

			if(! ($this->timezone != "" AND is_string($this->timezone)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "The settings must have a timezone.<br />";
			}
			
			return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting the settings.";
				$result['state'] = false;
				return $result;
			}
			
			$result['msg'] .= "The settings has been deleted succesfully.<br />";			
			return $result;
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function TimeToSend()
		{
			$dt_now = new DateTime();
			return !(strtotime($dt_now->format('H:i')) >=  SEND_START_HOUR &&	strtotime($dt_now->format('H:i')) <= SEND_END_HOUR);
		}	
				
	}	

?>