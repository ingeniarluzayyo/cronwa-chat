<?php
class Chat {
		
		public $tabla;
		public $id;
		public $campaign_id;
		public $message;
		public $sender_id;
		public $contact_id;
		public $datetime;
		public $type;
		public $sent;
		public $mode;
		
		private $array_masivo;
		
		public function __construct() {
			$this->tabla 					= 'chat';
			$this->id 						= '';
			$this->campaign_id				= '0';
			$this->message 					= '';
			$this->sender_id				= '';
			$this->contact_id				= '';
			$this->datetime					= '';
			$this->type 					= '';
			$this->sent 					= '0';
			$this->mode 					= '';
			
			$this->array_masivo             = array();
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			if(array_key_exists('campaign_id', $arr))
			{
				$this->campaign_id	= $arr['campaign_id'];
			}
			if(array_key_exists('message', $arr))
			{
				$this->message 		= $arr['message'];
			}
			if(array_key_exists('sender_id', $arr))
			{
				$this->sender_id	= $arr['sender_id'];
			}
			if(array_key_exists('contact_id', $arr))
			{
				$this->contact_id	= $arr['contact_id'];
			}
			if(array_key_exists('datetime', $arr))
			{
				$this->datetime		= $arr['datetime'];
			}
			if(array_key_exists('type', $arr))
			{
				$this->type 		= $arr['type'];
			}
			if(array_key_exists('sent', $arr))
			{
				$this->sent 		= $arr['sent'];
			}
			if(array_key_exists('mode', $arr))
			{
				$this->mode			= $arr['mode'];
			}
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {			
			$dt = new DateTime();	

			$arr = array(
				'campaign_id' 			=> DataBase::Cadena($this->campaign_id),
				'message' 				=> DataBase::Cadena($this->message),
				'sender_id' 			=> DataBase::Cadena($this->sender_id),
				'contact_id' 			=> DataBase::Cadena($this->contact_id),
				'datetime' 				=> DataBase::Cadena($dt->format('Y-m-d H:i:s')),
				'type' 					=> DataBase::Cadena($this->type),
				'sent' 					=> $this->sent,
				'mode' 					=> DataBase::Cadena($this->mode)
				);
			
			if(!Database::getInstance()->insert($arr, $this->tabla)) {
				return false;
			}				
			
			return true;
		}
		
		public function CrearBulk() {
			$dt = new DateTime();
			
			$arr = array(
				'campaign_id' 			=> DataBase::Cadena($this->campaign_id),
				'message' 				=> DataBase::Cadena($this->message),
				'sender_id' 			=> DataBase::Cadena($this->sender_id),
				'contact_id' 			=> DataBase::Cadena($this->contact_id),
				'datetime' 				=> DataBase::Cadena($dt->format('Y-m-d H:i:s')),
				'type' 					=> DataBase::Cadena($this->type),
				'sent' 					=> $this->sent,
				'mode' 					=> DataBase::Cadena($this->mode)
				);
			array_push($this->array_masivo,$arr);
			return true;
		}	
		
		public function Save()
		{
			if(!Database::getInstance()->insert_bulk($this->array_masivo, $this->tabla)) {
				return false;
			}
			
			return true;
		}
		
		public function Update_sent() {
			$arr = array(
				'sent' 	=> DataBase::Cadena('1')
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		
		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}
		
		public function CountSent($campaign = 0) {
			if($campaign != 0)
			{
				$this->campaign_id = $campaign;
			}
			
			$sql  = "SELECT count(1) as cant FROM ".$this->tabla." where campaign_id=".$this->campaign_id." AND sent=1 and mode='sent'";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["cant"];
		}
		
		public function CountTotal($campaign = 0) {
			if($campaign != 0)
			{
				$this->campaign_id = $campaign;
			}
			
			$sql  = "SELECT count(1) as cant FROM ".$this->tabla." where campaign_id=".$this->campaign_id." AND mode='sent'";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["cant"];
		}		

		public function GetById($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetEnvio($sender) {

			$sql  = "SELECT * FROM ".$this->tabla." WHERE sent=0 AND mode='sent' AND sender_id=".$sender." ORDER BY id asc LIMIT 0,1";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		
		public function GetAll($id_user = 0) {
			if($id_user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE contact_id =".$this->contact_id;	
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE contact_id =".$this->contact_id." AND sender_id IN (select id from senders where user=".$id_user.")";	
			}

			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Chat();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
}
?>