<?php
class Sender {
		
		public $tabla;
		public $id;
		public $numero;
		public $password;
		public $user;
		public $habilitado;
		public $status;
		public $profile_change;
		public $encuesta_id;
		
		public function __construct() {
			$this->tabla 					= 'senders';
			$this->id 						= 0;
			$this->numero					= '';
			$this->password 				= '';
			$this->user 					= '';
			$this->habilitado 				= '';
			$this->status 					= STATUS_WAITING;
			$this->profile_change 			= 'no';
			$this->encuesta_id 						= 0;
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->numero			= $arr['numero'];
			$this->password 		= $arr['password'];
			
			if(array_key_exists('user', $arr))
			{
				$this->user 			= $arr['user'];
			}
			if(array_key_exists('habilitado', $arr))
			{
				$this->habilitado		= $arr['habilitado'];
			}
			if(array_key_exists('status', $arr))
			{
				$this->status 			= $arr['status'];
			}
			if(array_key_exists('profile_change', $arr))
			{
				$this->profile_change 			= $arr['profile_change'];
			}
			if(array_key_exists('encuesta_id', $arr))
			{
				$this->encuesta_id 			= $arr['encuesta_id'];
			}
		}
		
			public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {			
			$dt = new DateTime();	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'numero' 				=> DataBase::Cadena($this->numero),
					'password' 				=> DataBase::Cadena($this->password),
					'user' 					=> DataBase::Cadena($this->user),
					'habilitado' 			=> DataBase::Cadena($this->habilitado),
					'status' 				=> DataBase::Cadena($this->status),
					'encuesta_id' 			=> $this->encuesta_id
					);
					if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a sender.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The sender has been created succesfully.<br />";
			return $result;
		}
		
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'numero' 				=> DataBase::Cadena($this->numero),
					'password' 				=> DataBase::Cadena($this->password),
					'user' 					=> DataBase::Cadena($this->user),
					'habilitado' 			=> DataBase::Cadena($this->habilitado)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
						$result['msg'] .= "Error while editing a sender.";
						$result['state'] = false;
						return $result;								
				}
			}else{
				return $result;
			}
			
			$result['msg'] .= "The sender has been edited succesfully.<br />";
			return $result;			
		 }
		 
		 
		 public function Update_available($available = -1) {
			if($available != -1)
			{
				$this->habilitado = $available;
			}
			
			$arr = array(
				'habilitado' 	=> DataBase::Cadena($this->habilitado)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		 
		 public function Update_profile_change($profile = -1) {
			if($profile != -1)
			{
				$this->profile_change = $profile;
			}
			
			$arr = array(
				'profile_change' 	=> DataBase::Cadena($this->profile_change)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		 
		 public function Update_status($status = '') {
			if($status != '')
			{
				$this->status = $status;
			}
			
			$arr = array(
				'status' 	=> DataBase::Cadena($this->status)
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] = "Error while updating the status.";
					$result['state'] = false;
					return $result;								
			}
		
			$result['msg'] = "The status has been updated successfully.";
			$result['state'] = true;
			return $result;		
		 }
		 
		 public function Update_encuesta($encuesta = -1) {
			if($encuesta != -1)
			{
				$this->encuesta_id = $encuesta;
			}
			
			$arr = array(
				'encuesta_id' 	=> $this->encuesta_id
				);
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					return false;								
			}
		
			return true;		
		 }
		 
		 
		  public function func() {
	        $debug = debug_backtrace();
	        return $debug[2]['function'];
	    } 
		 
		 public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->numero != "" AND is_numeric($this->numero)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on number.<br />";
			}
			
			if(($this->password == ""))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Password field is required.<br />";
			}
			
			if(($this->user == "0"))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "User field is required.<br />";
			}
			
			if (self::func() != 'Modificar'){
				$senders = $this->GetbyNumber();

				if(count($senders) >= 1)
				{				
					$result['state'] = false;
			 		$result['msg'] .= "The number has alredy exists on the database.<br />";
				}	
			}
					
			return $result;
		 }
		 
		 public function Eliminar() {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a sender.";
				$result['state'] = false;
				return $result;
			}
			$result['msg'] .= "The sender has been deleted succesfully.<br />";			
			return $result;	
		 }
			

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}
		
		public function GetUser() {
			$sql  = "SELECT username FROM usuarios where id=".$this->user;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["username"];
		}

		public function GetById($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetbyNumber($user = 0) {
						
			if($user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." WHERE numero like '%".$this->numero."%'";	
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE numero like '%".$this->numero."%' and user=".$user;	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Sender();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function GetCantContacts() {
			$sql  = "SELECT count(1) as cant FROM contacts WHERE sender_id=".$this->id." AND status= '".SUSCRIBE_STATUS."'";

			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}

			return $arr['cant'];								
		}
		
		public function GetFirstWaiting() {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE status = 'waiting' and habilitado='1' ORDER BY id LIMIT 0,1";
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function CheckPause($id = 0) {
			//False = finalizar
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return ($this->status != STATUS_PAUSE);
		}
		
		public function CheckProfilePicture($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return ($this->profile_change == 'yes');
		}
		
		public function CheckEncuesta($id = 0) {
			if($id != 0)
			{
				$this->id = $id;
			}
			
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $this->id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return $this->encuesta_id;
		}
		
		public function GetAllArr($user = 0) {
			if($user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." ORDER BY user";	
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE user=".$user." ORDER BY user";	
			}
			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();						
			
			while($row = mysqli_fetch_array($arr))
			{				
				$Contenidos[$row["id"]] = $row["numero"];
			}
						
			return $Contenidos;
		}
		
		
		public function GetAll($user = 0) {
			if($user == 0){
				$sql  = "SELECT * FROM ".$this->tabla." ORDER BY user";	
			}else{
				$sql  = "SELECT * FROM ".$this->tabla." WHERE user=".$user." ORDER BY user";	
			}
					
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Sender();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}
		
		public function ProfileImg($user)
		{
			$nombre = 'user_'.$user;
			$path = __DIR__.PAIT.'..'.PAIT.'img'.PAIT.'users'.PAIT;
			$foto = ManejoArchivos::existe_foto($nombre,$path);
			$img_sender =  $path.$foto;
			return $img_sender;
		}
		
		public function ParseoCSV($filepath) {
			$senders = array();
			$msg = "";
			$contenido = ManejoArchivos::contenido_archivo($filepath);

			$arr_contenido = explode("\n",$contenido);
			
			$i=1;
			foreach($arr_contenido as $linea)
			{
				$sender = explode(',',$linea);
				if(count($sender) == 2)
				{
					$newContenido = new Sender();
				
					$newContenido->numero 	= $sender[0];
					$newContenido->password 	= $sender[1];
					$newContenido->habilitado 	= 1;
					
					array_push($senders,$newContenido);
				}else{
					$linea = trim($linea);
					if(!empty($linea))
					{
						$msg .= "Error in line $i: ".$linea."<br />";	
					}
				}		
				$i++;		
			}
			
			$result['state'] = ($msg == ""); //No hubo errores
			$result['senders'] = $senders; //Senders extraidos
			$result['msg'] = $msg; //Mensaje de error
						
			return $result;
			
		}
		 
		public function ImportCSV($filepath) {
			$result = $this->ParseoCSV($filepath);
			
			if($result['state'] AND !empty($result['senders']))
			{
				foreach($result['senders'] as $sender)
				{
					$resultado = $sender->Crear();
					$result['state'] = $result['state'] && $resultado["state"];
					if(!$resultado['state'])
					{
						$result['msg'] .= "Error while importing sender: ".$sender->numero."<br />";
					}
				}
			}			
			
			return $result;
			
		}
		
		public function EscuharRespuestas()
		{
			try{						
				$w = new WhatsProt($this->numero, "WhatsApp", false);
				$w->eventManager()->bind("onGetMessage", "onMessage");			
				
				$w->Connect();			
				$w->LoginWithPassword($this->password);
				
				while($w->PollMessage());
			}catch(LoginFailureException $e_l){
				echo "[Listen]Login Faild Error <br />";
				//$this->habilitado = 0;
				//$this->Update_available();							
			}catch(Exceptio $e){
				//Para romper el bucle
			}					
		}
}

function onMessage($mynumber, $from, $id, $type, $time, $name, $body)
{	
	if($body != "")
	{			
		$numero = $number = explode('@',$from);
		$numero = $numero[0];
		
		$sender_number = $sender_number = explode('@',$mynumber);
		$sender_number = $sender_number[0];
		
		$fecha = new DateTime();	
		$fecha->setTimestamp($time);
		
		$sender = new Sender();
		$listen = new ListenSent();
		$sender->GetbyNumberListen($sender_number);
		
		$listen->contenido = $body;
		$listen->numero = $numero;
		$listen->sender = $sender->id;
		$listen->cluster = 0;
		$listen->fecha = $fecha->format('Y-m-d H:i:s');
		
		echo "[".$listen->numero."][".$listen->contenido."] -> Sender:[".$listen->sender."]<br />";
		
		$listen->Crear();
	}
}
?>