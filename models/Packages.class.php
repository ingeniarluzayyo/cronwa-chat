<?php
	class Packages {
		
		public $tabla;
		public $id;
		public $credits;
		public $amount;
		public $bg_color;
		
		public function __construct() {
			$this->tabla 			= 'packages';
			$this->id 				= '';
			$this->credits			= '';
			$this->amount				= '';
		}
		
		public function ParseoDeArray($arr) {
			if(array_key_exists('id', $arr))
			{
				$this->id 			= $arr['id'];
			}
			$this->credits			= $arr['credits'];
			$this->amount				= $arr['amount'];
			$this->bg_color				= $arr['bg_color'];
		}
		
		public function setTabla($tabla)
		{
			$this->tabla = $tabla;
		}

		public function Crear() {	
			$result = $this->ValidarDatos();	
			if($result['state'])
			{		
				$arr = array(
					'credits' 	=> $this->credits,
					'amount' 		=> DataBase::Cadena($this->amount),
					'bg_color' 		=> DataBase::Cadena($this->bg_color)
					);
				if(!Database::getInstance()->insert($arr, $this->tabla)) {
					$result['msg'] .= "Error while creating a Package.";
					$result['state'] = false;
					return $result;
				}				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The package has been created succesfully.<br />";
			return $result;
		 }
		 
		public function Modificar() {
			$result = $this->ValidarDatos();	
			if($result['state'])
			{
				$arr = array(
					'credits' 	=> $this->credits,
					'amount' 		=> DataBase::Cadena($this->amount),
					'bg_color' 		=> DataBase::Cadena($this->bg_color)
					);
				$where = "WHERE id = " . $this->id;
				if(!Database::getInstance()->update($arr, $this->tabla, $where)) {
					$result['msg'] .= "Error while editing a package.";
					$result['state'] = false;
					return $result;
				}
				
			}else{
				return $result;
			}
			
			$result['msg'] .= "The package has been edited succesfully.<br />";
			return $result;
		 }
		
		public function ValidarDatos()
		 {
		 	$result['state'] = true;
		 	$result['msg'] = "";
		 	
		 	if(! ($this->amount != "" AND is_numeric($this->amount)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on amount.<br />";
			}
			if(! ($this->credits != "" AND is_numeric($this->credits)))
		 	{
				$result['state'] = false;
		 		$result['msg'] .= "Invalid characters on credits.<br />";
			}			
						
			return $result;
		 }
		 
		public function Eliminar() {
			$result['state'] = true;
			$result['msg'] 	 = "";
			
			$where = "WHERE id = " . $this->id;
			if(!Database::getInstance()->delete($this->tabla, $where)) {
				$result['msg'] .= "Error while deleting a package.";
				$result['state'] = false;
				return $result;
			}
			
			$result['msg'] .= "The package has been deleted succesfully.<br />";			
			return $result;			
		 }

		 public function newId() {
			$sql  = "SELECT max(id)+1 as id FROM ".$this->tabla;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return 0;
			}			
		
			return $arr["id"];
		}

		public function GetById($id) {
			$sql  = "SELECT * FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}

			$this->ParseoDeArray($arr);			
		
			return true;
		}	
		
		public function GetCredits($id) {
			$sql  = "SELECT credits FROM ".$this->tabla." WHERE id = " . $id;
			if(!$arr = Database::getInstance()->GetUnicoQuery($sql)) {
				return false;
			}			
		
			return $arr['credits'];
		}		

		public function GetAll() {
			$sql  = "SELECT * FROM ".$this->tabla." ORDER BY id";			
			if(!$arr = Database::getInstance()->executeQuery($sql)) {
				return false;
			}
			$Contenidos = array();
			
			//$oUsuario->ParseoDeArray($arr);
			while($row = mysqli_fetch_array($arr))
			{
				$newContenido = new Packages();
				$newContenido->ParseoDeArray($row);
				array_push($Contenidos,$newContenido);
			}
						
			return $Contenidos;
		}

		
	}	

?>